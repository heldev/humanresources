<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		
		<link rel="stylesheet" type="text/css" media="all"
			href="javascript/calendar/calendar-win2k-cold-1.css">
		<script type="text/javascript" src="javascript/calendar/cal.js"></script>
		<script type="text/javascript" src="javascript/comm/comm.js"></script>
		<script type="text/javascript" src="javascript/comm/list.js"></script>
		<script type="text/javascript" src="../jquery/jquery-1.8.0.js"></script>
		<link rel="stylesheet" href="../css/table.css" type="text/css"></link>
		
		
	<script type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript">
	$(document).ready(function() {
	
	$("#dep").click(function() {
		var depId = $(this).val();
		$.post("/humanresources/filesj/majorAction_getByIdMajor.action", {
			pid : depId
		}, function(data) {
			var str = "";
			for ( var i = 0; i < data.dtos.length; i++) {
				var d = data.dtos[i];
				
				str += "<option value=" + d.id + " >" + d.name + "</option>"
			}
			$("#maj").html(str);
		}, "json");
	});

});
 		</script>
 		</head>

	<body>
		<form name="humanfileForm" method="post" action="/humanresources/selectFiles/employeeAction_selectEmployee.action">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--人力资源档案管理--人力资源档案查询</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						
						<input type="submit" value="搜索" class="BUTTON_STYLE1" >
					</td>
				</tr>
			</table>
			<table width="100%" border="1" class="TABLE_STYLE1">
				
				<tr class="TR_STYLE1">
					<td width="16%" class="TD_STYLE1">
						请选择员工所在部门
					</td>
					<td width="84%" class="TD_STYLE2">
						<select name="prop.depId" size="5" id="dep" class="SELECT_STYLE2">
						<c:forEach items="${request.deps}"  var="d">
								<option value="${d.hrDepId}">
									${d.hrDepName}
								</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr class="TR_STYLE1">
					<td width="16%" class="TD_STYLE1">
						请选择职位名称
					</td>
					<td width="84%" class="TD_STYLE2">
						<select name="prop.majId" id="maj" size="5" class="SELECT_STYLE2">
						
						</select>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						请输入建档时间
					</td>
					<td width="84%" class="TD_STYLE2">
						<input type="text" name="prop.startTime" value="" style="width:14%"   class="SELECT_STYLE2" class="Wdate" onclick="WdatePicker()" class="INPUT_STYLE2" id="date_start">
						
						
						至<input type="text" name="prop.endTime" value="" style="width:14% "  class="SELECT_STYLE2" class="Wdate" onclick="WdatePicker()" class="INPUT_STYLE2" id="date_end">
						（YYYY-MM-DD）
					</td>
				</tr>
				
			</table>
		</form>
	</body>
</html>

