<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<link rel="stylesheet" href="../css/table.css" type="text/css"></link>
	
	
	<script type="text/javascript" src="../jquery/jquery-1.8.0.js"></script>
		<script type="text/javascript">
	
	
	function updateEmp(a) {
	$.post("/humanresources/filesj/salaryAction_getByIdSalary.action", {
		sid : a
	}, function(data) {
		var str = "";
		
		for ( var i = 0; i < data.dts.length; i++) {
			var d = data.dts[i];
			str += "<option value=" + d.id + " >" + d.name + "</option>"
		}
		$("#sal").html(str);
	}, "json");
}
		</script>
	</head>

	<body>
		<form action="humanfile.do" method="post">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--人力资源档案管理--人力资源档案删除管理--人力资源档案永久删除</font>
					</td>
				</tr>
				<tr>
					<td>
						符合条件的人力资源档案总数：0例
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr class="TR_STYLE1">
				
					<td width="14%" height="13" class="TD_STYLE1">
						档案编号
					</td>
					<td width="11%" class="TD_STYLE1">
						姓名
					</td>
					<td width="6%" class="TD_STYLE1">
						性别
					</td>
					<td width="13%" class="TD_STYLE1">
						所属部门
					</td>
					<td width="12%" class="TD_STYLE1">
						职位名称
					</td>

					<td width="7%" class="TD_STYLE1">
						操作
					</td>
					
				</tr>
				<c:forEach items="${empList}" var="emp">
				<tr  >
				<td width="14%" height="13" class="TD_STYLE2">
						${emp.hrEmpId}
					</td>
					<td width="11%" class="TD_STYLE2">
						${emp.hrEmpName}
					</td>
					<td width="6%" class="TD_STYLE2">
						${emp.hrEmpSex}
					</td>
					<td width="13%" class="TD_STYLE2">
					
						${emp.hrDepartment.hrDepName}
					</td>
				
					<td width="13%" class="TD_STYLE2">
						${emp.hrMajor.hrMajName}
					</td>
					<td width="7%" class="TD_STYLE2">
						<a href="/humanresources/filess/employeeAction_deletesEmployee.action?empId=${emp.hrEmpId}">永久删除</a>
						</td>
				</tr>
				</c:forEach>
			</table>
			<p>&nbsp;&nbsp;总数：${count}例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="images/go.bmp" width=18 height=18 border=0>
		</form>
	</body>
</html>
