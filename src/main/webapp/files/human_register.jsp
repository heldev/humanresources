<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<link rel="stylesheet" href="table.css" type="text/css">


		<link rel="stylesheet" type="text/css" media="all"
			href="javascript/calendar/calendar-win2k-cold-1.css">
		<script type="text/javascript" src="javascript/calendar/cal.js">
</script>
		<script type="text/javascript" src="javascript/comm/comm.js">
</script>
		<script type="text/javascript" src="javascript/comm/select.js">
</script>
		<script type="text/javascript" src="../jquery/jquery-1.8.0.js">
</script>

<link rel="stylesheet" href="../css/table.css" type="text/css"></link>
		
		
	<script type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript">
var subcat = new Array(2);
subcat[0] = [ "1", "01/软件公司", "01/集团", "01/集团/01/软件公司" ];
subcat[1] = [ "2", "02/生物科技有限公司", "01/集团", "01/集团/02/生物科技有限公司" ];
var subcat1 = new Array(2);
subcat1[0] = [ "1", "01/外包组", "01/集团/01/软件公司" ];
subcat1[1] = [ "2", "01/药店", "01/集团/02/生物科技有限公司" ];
var subcat2 = new Array(8);
subcat2[0] = [ "1", "01/区域经理", "01/销售" ];
subcat2[1] = [ "2", "02/总经理", "01/销售" ];
subcat2[2] = [ "3", "01/项目经理", "02/软件开发" ];
subcat2[3] = [ "4", "02/程序员", "02/软件开发" ];
subcat2[4] = [ "5", "01/人事经理", "03/人力资源" ];
subcat2[5] = [ "6", "02/专员", "03/人力资源" ];
subcat2[6] = [ "7", "01/主任", "04/生产部" ];
subcat2[7] = [ "8", "02/技术工人", "04/生产部" ];



function getSalary(a) {
	$.post("/humanresources/filesj/salaryAction_getByIdSalary.action", {
		sid : a
	}, function(data) {
		var str = "";
		
		for ( var i = 0; i < data.dts.length; i++) {
			var d = data.dts[i];
			str += "<option value=" + d.id + " >" + d.name + "</option>"
		}
		$("#sal").html(str);
	}, "json");
}


$(document).ready(function() {

	$("#depart").click(function() {

		var depId = $(this).val();
		$.post("/humanresources/filesj/majorAction_getByIdMajor.action", {
			pid : depId
		}, function(data) {
			var str = "";
			for ( var i = 0; i < data.dtos.length; i++) {
				var d = data.dtos[i];
				
				str += "<option value=" + d.id + " onclick='getSalary("+d.id+")'>" + d.name + "</option>"
			}
			$("#maj").html(str);
		}, "json");
	});

});







</script>
		</head>

	<body>
		<form name="humanfileForm" method="post" action="../addFiles/employeeAction_addEmployee.action">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">gligl您正在做的业务是：人力资源--人力资源档案管理--人力资源

							档案登记 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="提交" class="BUTTON_STYLE1">
						<input type="reset" value="清除" class="BUTTON_STYLE1">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>


					<td class="TD_STYLE1">
						部门名称
					</td>
					<td class="TD_STYLE2">
						<select name="depId" id="depart" class="SELECT_STYLE1">
						
							<c:forEach items="${request.deps}"  var="d">
								<!-- onclick="getMajor('${d.hrDepId}')" -->
								<option value="${d.hrDepId}">
									${d.hrDepName}
								</option>
							</c:forEach>
						</select>
					</td>
					<td class="TD_STYLE1">
						职位名称
					</td>

					<td class="TD_STYLE2" >
						<select name="majId" id="maj" class="SELECT_STYLE1">
						
						</select>
					</td>





					<td width="11%" class="TD_STYLE1">
						薪酬标准
					</td>
					<td class="TD_STYLE2" width="280">
						<select name="salId" id="sal" class="SELECT_STYLE1">
						
						</select>
					</td>
					<td rowspan="4">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						姓名
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpName" value=""
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						性别
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpSex" class="SELECT_STYLE1">
							<option value="男">
								男

							</option>
							<option value="女">
								女
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						年龄
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpAge" value=""
							class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						婚姻状况
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpMarriage" class="SELECT_STYLE1">
							<option value="已婚">
								已婚

							</option>
							<option value="未婚">
								未婚
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						民族
					</td>
					<td class="TD_STYLE2" >
						<select name="emp.hrEmpFolk" class="SELECT_STYLE1">
							<option value="汉族">
								汉族
							</option>

							<option value="回族">
								回族
							</option>
							<option value="蒙古族">
								蒙古族
							</option>

							<option value="藏族">
								藏族
							</option>
							<option value="维吾尔族">
								维吾尔族
							</option>

							<option value="满族">
								满族
							</option>
							<option value="朝鲜族">
								朝鲜族
							</option>

							<option value="壮族">
								壮族
							</option>
							<option value="布依族">
								布依族
							</option>

							<option value="侗族">
								侗族
							</option>
							<option value="汉族">
								汉族
							</option>

							<option value="高山族">
								高山族
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						EMAIL
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="emp.hrEmpEmail" value=""
							class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						手机
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpPhone" value=""
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						QQ
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpQq" value=""
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						入职时间
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="emp.hrEmpJobDater" 
							class="INPUT_STYLE2" class="Wdate" onclick="WdatePicker()">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						住址
					</td>
					<td  class="TD_STYLE2">
						<input type="text" name="emp.hrEmpAddress" 
							class="INPUT_STYLE2">
					</td>
									
					<td class="TD_STYLE1">
						身份证号码
					</td>
					<td class="TD_STYLE2" colspan="0">
						<input type="text" name="emp.hrEmpCard" 
							class="INPUT_STYLE2">
					</td>	
									
									
										
					<td class="TD_STYLE1">
						邮编
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="emp.hrEmpPostcode" 
							class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						籍贯
					</td>
					<td class="TD_STYLE2">
						
						<input type="text" name="emp.hrEmpNative" class="INPUT_STYLE2"/>
					</td>
					<td class="TD_STYLE1">
						出生地
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="" value=""
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						出生日期
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpBirthday" 
							class="INPUT_STYLE2" class="Wdate" onclick="WdatePicker()">
					</td>


				</tr>
				<tr>
					<td class="TD_STYLE1">
						宗教信仰
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpReligion" class="SELECT_STYLE1">
							<option value="无">
								无
							</option>

							<option value="佛教">
								佛教
							</option>
							<option value="基督教">
								基督教
							</option>
							<option value="伊斯兰教">
								伊斯兰教
							</option>
							<option value="道教">
								道教
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						政治面貌
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpGovernment" class="SELECT_STYLE1">
							<option value="党员">
								党员
							</option>
							<option value="群众">
								群众
							</option>
							<option value="共青团员">
								共青团员
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						&nbsp;
					</td>
					<td class="TD_STYLE2" colspan="2">
						&nbsp;
					</td>

				</tr>
				<tr>
					<td class="TD_STYLE1">
						爱好
					</td>
					<td class="TD_STYLE2">
							<input type="text" name="emp.hrEmpHobby"  class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						学历
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpDegree" class="SELECT_STYLE1">
							<option value="博士">
								博士
							</option>
							<option value="硕士">
								硕士
							</option>
							<option value="本科">
								本科
							</option>
							<option value="大专">
								大专
							</option>
							<option value="高中">
								高中
							</option>
						</select>
					</td>

					<td class="TD_STYLE1">
						专业
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpEducated"  class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						登记人
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpRegisterMan" value="better_wanghao"
							readonly="readonly" class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						开户行
					</td>
					<td class="TD_STYLE2">
							<select name="emp.hrEmpOpenBank" class="SELECT_STYLE1">
							<option value="中国工商银行">
								中国工商银行
							</option>

							<option value="中国银行">
								中国银行
							</option>
							<option value="中国建设银行">
								中国建设银行
							</option>

							<option value="中国农业银行">
								中国农业银行
							</option>
							<option value="中国交通银行">
								中国交通银行
							</option>

							<option value="中国邮政银行">
								中国邮政银行
							</option>
							<option value="中国中信银行">
								中国中信银行
							</option>

							<option value="中国兴业银行">
								中国兴业银行
							</option>

							<option value="中国民生银行">
								中国民生银行
							</option>
							<option value="中国广大银行">
								中国广大银行
							</option>

							<option value="中国广发银行">
								中国广发银行
							</option>
							<option value="中国浦发银行">
								中国浦发银行
							</option>

							<option value="北京银行">
								北京银行
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						&nbsp;
					</td>
					<td class="TD_STYLE2">
						&nbsp;
					</td>

				</tr>
				<tr>
					<td class="TD_STYLE1">
						建档时间
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpRegistTime"
							class="INPUT_STYLE2" class="Wdate" onclick="WdatePicker()">
					</td>
					<td class="TD_STYLE1">
						特长
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpSpeciaity"
							class="INPUT_STYLE2" >
						
					</td>
					<td class="TD_STYLE1">
						照片上传
					</td>
					<td class="TD_STYLE2">
						<input type="file" name="emp.hrEmpPhoto" value="浏览" />
					</td>

				</tr>
				<tr>
					<td class="TD_STYLE1">
						个人简介
					</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="emp.hrEmpResume" rows="4"
							class="TEXTAREA_STYLE1"></textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						备注
					</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="emp.hrEmpRemarks" rows="4" class="TEXTAREA_STYLE1"></textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
	<script type="text/javascript">
Calendar.setup( {
	inputField : "date_start",
	ifFormat : "%Y-%m-%d",
	showsTime : false,
	button : "date_start",
	singleClick :

	true,
	step : 1
});
Calendar.setup( {
	inputField : "date_end",
	ifFormat : "%Y-%m-%d",
	showsTime : false,
	button : "date_end",
	singleClick :

	true,
	step : 1
});
</script>
</html>

