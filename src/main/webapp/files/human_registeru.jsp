<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<link rel="stylesheet" href="table.css" type="text/css">


		<link rel="stylesheet" type="text/css" media="all"
			href="javascript/calendar/calendar-win2k-cold-1.css">
		<script type="text/javascript" src="javascript/calendar/cal.js">
</script>
		<script type="text/javascript" src="javascript/comm/comm.js">
</script>
		<script type="text/javascript" src="javascript/comm/select.js">
</script>
		<script type="text/javascript" src="../jquery/jquery-1.8.0.js">
</script>

<link rel="stylesheet" href="../css/table.css" type="text/css"></link>
		
		
	<script type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript">



</script>
		</head>

	<body>
		<form name="humanfileForm" method="post" action="../updateFiles/employeeAction_upEmployee.action">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--人力资源档案管理--人力资源

							档案变更 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="变更" class="BUTTON_STYLE1">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>


					<td class="TD_STYLE1">
						部门名称
					</td>
					<td class="TD_STYLE2">
						<select name="depId"  class="SELECT_STYLE1">
						
								<option value="${emp.hrDepartment.hrDepId}" >
									${emp.hrDepartment.hrDepName}
								</option>
							
						</select>
					</td>
					<td class="TD_STYLE1">
						职位名称
					</td>

					<td class="TD_STYLE2" >
						<select name="majId"  class="SELECT_STYLE1" >
						<option value="${emp.hrMajor.hrMajId}">
									${emp.hrMajor.hrMajName}
								</option>
						</select>
					</td>





					<td width="11%" class="TD_STYLE1">
						薪酬标准
					</td>
					<td class="TD_STYLE2" width="280">
						<select name="salId" id="sal" class="SELECT_STYLE1" >
						<option value="${emp.hrSalary.hrSalId}">
									${emp.hrSalary.hrSalName}
								</option>
						</select>
					</td>
					<td rowspan="4">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						姓名
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpName" value="${emp.hrEmpName}" 
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						性别
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpSex" class="SELECT_STYLE1">
							<option value="${emp.hrEmpSex}">
								${emp.hrEmpSex}
							</option>
							
						</select>
					</td>
					<td class="TD_STYLE1">
						年龄
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpAge" value="${emp.hrEmpAge}"
							class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						婚姻状况
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpMarriage" class="SELECT_STYLE1">
							<option value="已婚">
								已婚

							</option>
							<option value="未婚">
								未婚
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						民族
					</td>
					<td class="TD_STYLE2" >
						<select name="emp.hrEmpFolk" class="SELECT_STYLE1">
							<option value="汉族">
								汉族
							</option>

							<option value="回族">
								回族
							</option>
							<option value="蒙古族">
								蒙古族
							</option>

							<option value="藏族">
								藏族
							</option>
							<option value="维吾尔族">
								维吾尔族
							</option>

							<option value="满族">
								满族
							</option>
							<option value="朝鲜族">
								朝鲜族
							</option>

							<option value="壮族">
								壮族
							</option>
							<option value="布依族">
								布依族
							</option>

							<option value="侗族">
								侗族
							</option>
							<option value="汉族">
								汉族
							</option>

							<option value="高山族">
								高山族
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						EMAIL
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="emp.hrEmpEmail" value="${emp.hrEmpEmail}"
							class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						手机
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpPhone" value="${emp.hrEmpPhone}"
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						QQ
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpQq" value="${emp.hrEmpQq}"
							class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						入职时间
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="emp.hrEmpJobDater" 
							class="INPUT_STYLE2" class="Wdate" readonly="readonly" value="${emp.hrEmpJobDater}">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						住址
					</td>
					<td  class="TD_STYLE2">
						<input type="text" name="emp.hrEmpAddress" value="${emp.hrEmpAddress}"
							class="INPUT_STYLE2">
					</td>
									
					<td class="TD_STYLE1">
						身份证号码
					</td>
					<td class="TD_STYLE2" colspan="0">
						<input type="text" name="emp.hrEmpCard" value="${emp.hrEmpCard}" readonly="readonly"
							class="INPUT_STYLE2">
					</td>	
									
									
										
					<td class="TD_STYLE1">
						邮编
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="emp.hrEmpPostcode" value="${emp.hrEmpPostcode}"
							class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						籍贯
					</td>
					<td class="TD_STYLE2">
						
						<input type="text" name="emp.hrEmpNative" value="${emp.hrEmpNative}" readonly="readonly" class="INPUT_STYLE2"/>
					</td>
					<td class="TD_STYLE1">
						&nbsp;
					</td>
					<td class="TD_STYLE2">
						&nbsp;
					</td>
					<td class="TD_STYLE1">
						出生日期
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpBirthday" value="${emp.hrEmpBirthday}" readonly="readonly"
							class="INPUT_STYLE2" >
					</td>


				</tr>
				<tr>
					<td class="TD_STYLE1">
						宗教信仰
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpReligion" class="SELECT_STYLE1">
							<option value="无">
								无
							</option>

							<option value="佛教">
								佛教
							</option>
							<option value="基督教">
								基督教
							</option>
							<option value="伊斯兰教">
								伊斯兰教
							</option>
							<option value="道教">
								道教
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						政治面貌
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpGovernment"  class="SELECT_STYLE1">
							<option value="党员">
								党员
							</option>
							<option value="群众">
								群众
							</option>
							<option value="共青团员">
								共青团员
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						&nbsp;
					</td>
					<td class="TD_STYLE2" colspan="2">
						&nbsp;
					</td>

				</tr>
				<tr>
					<td class="TD_STYLE1">
						爱好
					</td>
					<td class="TD_STYLE2">
							<input type="text" name="emp.hrEmpHobby" value="${emp.hrEmpHobby}"  class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						学历
					</td>
					<td class="TD_STYLE2">
						<select name="emp.hrEmpDegree" class="SELECT_STYLE1">
							<option value="博士">
								博士
							</option>
							<option value="硕士">
								硕士
							</option>
							<option value="本科">
								本科
							</option>
							<option value="大专">
								大专
							</option>
							<option value="高中">
								高中
							</option>
						</select>
					</td>

					<td class="TD_STYLE1">
						专业
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpEducated" value="${emp.hrEmpEducated}" class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						登记人
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpRegisterMan" value="${emp.hrEmpRegisterMan}"
							readonly="readonly" class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						开户行
					</td>
					<td class="TD_STYLE2">
							<select name="emp.hrEmpOpenBank" class="SELECT_STYLE1">
							<option value="中国工商银行">
								中国工商银行
							</option>

							<option value="中国银行">
								中国银行
							</option>
							<option value="中国建设银行">
								中国建设银行
							</option>

							<option value="中国农业银行">
								中国农业银行
							</option>
							<option value="中国交通银行">
								中国交通银行
							</option>

							<option value="中国邮政银行">
								中国邮政银行
							</option>
							<option value="中国中信银行">
								中国中信银行
							</option>

							<option value="中国兴业银行">
								中国兴业银行
							</option>

							<option value="中国民生银行">
								中国民生银行
							</option>
							<option value="中国广大银行">
								中国广大银行
							</option>

							<option value="中国广发银行">
								中国广发银行
							</option>
							<option value="中国浦发银行">
								中国浦发银行
							</option>
							<option value="北京银行">
								北京银行
							</option>
						</select>
					</td>
					<td class="TD_STYLE1">
						&nbsp;
					</td>
					<td class="TD_STYLE2">
						&nbsp;
					</td>

				</tr>
				<tr>
					<td class="TD_STYLE1">
						建档时间
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpRegistTime"
							class="INPUT_STYLE2" value="${emp.hrEmpRegistTime}" readonly="readonly">
					</td>
					<td class="TD_STYLE1">
						特长
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="emp.hrEmpSpeciality" value="${emp.hrEmpSpeciality}"
							class="INPUT_STYLE2" >
						
					</td>
					<td class="TD_STYLE1">
						照片上传
					</td>
					<td class="TD_STYLE2">
						<input type="file"  name="emp.hrEmpPhoto" value="浏览" />
					</td>

				</tr>
				<tr>
					<td class="TD_STYLE1">
						个人简介
					</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="emp.hrEmpResume" rows="4"
							class="TEXTAREA_STYLE1" >${emp.hrEmpResume}</textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						备注
					</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="emp.hrEmpRemarks" rows="4" class="TEXTAREA_STYLE1">${emp.hrEmpRemarks}</textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
	<script type="text/javascript">
Calendar.setup( {
	inputField : "date_start",
	ifFormat : "%Y-%m-%d",
	showsTime : false,
	button : "date_start",
	singleClick :

	true,
	step : 1
});
Calendar.setup( {
	inputField : "date_end",
	ifFormat : "%Y-%m-%d",
	showsTime : false,
	button : "date_end",
	singleClick :

	true,
	step : 1
});
</script>
</html>

