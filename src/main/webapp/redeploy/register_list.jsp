<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<!-- 导入table.css -->	
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css"></link>
	
	</head>
	<body onLoad="">
		<form action="" method="post" >
			<font color="#0080ff"> 您在做的业务是-人力资源-调动管理-调动登记列表 </font>
			<br/><br/><br/>			

			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr height="21">
					<td width="19%" class="TD_STYLE1">部门名称</td>
					<td width="19%" class="TD_STYLE1">
						部门职位</td>
					<td width="19%" class="TD_STYLE1">
						薪酬标准
					</td>
					<td width="19%" class="TD_STYLE1">
						名字
					</td>
					<td width="5%" class="TD_STYLE1">
						调动
					</td>

				</tr>
				<s:iterator value="#request.blurQuery">
					<tr height="21">
						<td class="TD_STYLE2">
						<s:property value="hrDepartment.hrDepName"/>
						</td>
						<td class="TD_STYLE2">
						<s:property value="hrMajor.hrMajName"/>
						</td>
						<td class="TD_STYLE2">
						<s:property value="hrSalary.hrSalName"/>
						</td>
						<td class="TD_STYLE2">
						<s:property value="hrEmpName"/>
						</td>
						<td class="TD_STYLE2">
						<!-- 此超链转到调动 -->
							<a href="/humanresources/move/registerMove.action?id=${hrEmpId}">调动</a>
						</td>
					</tr>
				</s:iterator>	
			</table>
		  <br>
			<br>
			
		</form>
	</body>
</html>