<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 根据id查询详细信息页面 -->
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css"></link>
		<!-- 导入jquery.js -->
		<SCRIPT type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></SCRIPT>
		<!-- 导入check.js -->
		<script type="text/javascript" src="/humanresources/javascript/redeploy/check.js" charset="gbk"></script>
		<!-- 导入日期控件js -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		
	</head>
<body>
		<form method="post" id="form1">
		  <table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源--调动管理--调动审核</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="button" class="BUTTON_STYLE1" onClick="javascript:window.history.back();" value="返回">
					</td>
				</tr>
			</table>
		<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr height="21">
					<td class="TD_STYLE1" width="8%">
						档案编号
					</td>
					<td class="TD_STYLE2" width="10%">
						<input type="text" name="move.hrEmployee.hrEmpId"  value="${remove.hrEmployee.hrEmpId}" class="INPUT_STYLE2">
					</td>					
					<td class="TD_STYLE1" width="12%">
						姓名
					</td>					
					<td class="TD_STYLE2" width="10%">
					<input type="text" name="mc.firstKindName"  value="${remove.hrEmployee.hrEmpName}" class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1" width="12%">						
					</td>
					<td class="TD_STYLE2" width="10%">						
					</td>
					<td class="TD_STYLE1" width="8%">
					</td>
					<td class="TD_STYLE2" width="10%">
					</td>
				</tr>

				<tr>
					<td class="TD_STYLE1">
						原部门分类
					</td>
					<td class="TD_STYLE2">
						
						<input type="text" name="move.hrDepOldName" readonly="readonly" value="${remove.hrDepOldName}" class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1" width="12%">
						原职位名称
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="move.hrMajOldName" readonly="readonly" value="${remove.hrMajOldName}" class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						原薪酬标准
					</td>
					<td class="TD_STYLE2">
						<input type="text"  readonly="readonly" value="${remove.hrEmployee.hrSalary.hrSalBase}" class="INPUT_STYLE2">
					</td>
					<td class="TD_STYLE1">
						原薪酬总额
					</td>
					<td class="TD_STYLE2" >
						<input type="text" readonly="readonly" value="${remove.hrEmployee.hrSalary.hrSalRental}" class="INPUT_STYLE2">
					</td>
				</tr>				
				<tr>
					<td class="TD_STYLE1">新部门分类</td>
					<td class="TD_STYLE2">
						
						<input class="INPUT_STYLE2"  value="${remove.hrDepartment.hrDepName}">
					</td>
					<td class="TD_STYLE1" width="12%">
					新职位名称</td>
					<td class="TD_STYLE2">
						<input class="INPUT_STYLE2" value="${remove.hrMajor.hrMajName}"/>
					</td>
					<td class="TD_STYLE1">
						新薪酬标准
					</td>
					<td class="TD_STYLE2">
						<s:property value="#remove.hrEmployee.hrSalary.hrSalBase"/>
					</td>
					<td class="TD_STYLE1"> 新薪酬总额 </td>
					<td class="TD_STYLE2"><input type="text" name="move.hrEmployee.hrSalRental" id="nsal" value="${remove.hrEmployee.hrSalary.hrSalRental}" readonly="readonly"  class="INPUT_STYLE2"></td>
				</tr>
				<tr>
				  <td class="TD_STYLE1"> 登记人 </td>
				  <td class="TD_STYLE2"><input type="text" value="${remove.hrRemRegisterMan}" class="INPUT_STYLE2"></td>
				  <td class="TD_STYLE1"> 登记时间 </td>
				  <td class="TD_STYLE2"><input type="text" name="move.hrRemRegisterTime" readonly="readonly" id="Tdate" class="INPUT_STYLE2" value="${remove.hrRemRegisterTime}"></td>
					<td class="TD_STYLE1">&nbsp;</td>
					<td class="TD_STYLE2">&nbsp;</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						调动原因
					</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="move.hrRemCause" rows="6" class="TEXTAREA_STYLE1">${remove.hrRemCause}</textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

