<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!-- 调动查询成功页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>

		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css"></link>
				
	</head>

	<body>
		<form method="post" action="">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--调动管理--调动查询</font>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						当前已复核的调动人数:
						例
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				bordercolorlight=#848284 bordercolordark=#eeeeee
				class="TABLE_STYLE1">
				<tr height="21">
					<td width="25%" class="TD_STYLE1">
						<span>姓名</span>
					</td>
					<td width="25%" class="TD_STYLE1">
						<span>登记人</span>
					</td>
					<td width="30%" class="TD_STYLE1">
						<span>登记时间</span>
					</td>
					<td width="20%" class="TD_STYLE1">
						<span>查看详情</span>
					</td>

				</tr>
				<s:iterator value="#request.remove">
					<tr class="TD_STYLE2" height="21">
						<td>
						<s:property value="hrEmployee.hrEmpName"/>
						</td>
						<td>
						<s:property value="hrRemRegisterMan"/>
						</td>
						<td>
						<s:property value="hrRemRegisterTime"/>
						</td>
						<td>
							<!-- 根据此id查询详细信息页面 -->
							<a href="/humanresources/move/detailMove.action?id=${hrRemId}">查看</a>
						</td>
					</tr>
				</s:iterator>
			</table>
			<div class="pager">
			<p>&nbsp;&nbsp;总数：6例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="../../images/go.bmp" width=18 height=18 border=0>
			</p>
			</div>
</form>
	</body>
</html>
