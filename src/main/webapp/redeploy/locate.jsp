<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!-- 调动查询成功页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css"></link>
		<!-- 导入jquery.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
		<!-- 导入locate.js -->
		<script type="text/javascript" src="/humanresources/javascript/redeploy/locate.js"></script>
		<!-- 导入日期控件js -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		
	</head>

	<body>
	<!-- form提交到 -->
		<form method="post" action="/humanresources/move/moveSelect.action">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--调动管理--调动查询 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="查询" class="BUTTON_STYLE1" >
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>
					<td class="TD_STYLE1">
						请选择职位分类
					</td>
					<td width="84%" class="TD_STYLE2">
						<select name="qp.depId" size="5" id="dep"  class="SELECT_STYLE2">							
								<s:iterator value="#request.department"><option value="${hrDepId}"><s:property value="hrDepName"/></option></s:iterator>							
						</select>
					</td>
				</tr>
				<tr class="TR_STYLE1">
					<td width="16%" class="TD_STYLE1">
						请选择职位名称
					</td>
					<td width="84%" class="TD_STYLE2">
						<select name="qp.majId" size="5" id="maj"  class="SELECT_STYLE2">							
								
						</select>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						请输入建档时间
					</td>
					<td width="84%" class="TD_STYLE2">
						<input type="text" name="qp.startTime" value="" style="width:14% " class="INPUT_STYLE2 Wdate" onclick="WdatePicker()" id="date_start">至<input type="text" name="qp.endTime" value="" style="width:14% " class="INPUT_STYLE2 Wdate" onclick="WdatePicker()" id="date_end">
						（YYYY-MM-DD）
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
