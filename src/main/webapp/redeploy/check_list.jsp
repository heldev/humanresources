<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 审核查询成功页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css"></link>
		
	</head>

	<body>
		<form method="post" action="">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--调动管理--调动审核列表</font>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
						
					</td>
				</tr>
				<tr>
					<td>
						当前待复核的调动人数:<s:property value="#request.remove.size()"/>
						例
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr height="21">
					<td width="19%" class="TD_STYLE1">所属部门</td>
					<td width="19%" class="TD_STYLE1">
						所属职位</td>
					<td width="19%" class="TD_STYLE1">
						薪酬标准
					</td>
					<td width="19%" class="TD_STYLE1">
						名字
					</td>
					<td width="5%" class="TD_STYLE1">
						审核
					</td>

				</tr>
				<s:iterator value="#request.remove">
					<tr height="21">
						<td class="TD_STYLE2">
						<s:property value="hrEmployee.hrDepartment.hrDepName"/>
						</td>
						<td class="TD_STYLE2">
						<s:property value="hrEmployee.hrMajor.hrMajName"/>
						</td>
						<td class="TD_STYLE2">
						<s:property value="hrEmployee.hrSalary.hrSalBase"/>
						</td>
						<td class="TD_STYLE2">
						<s:property value="hrEmployee.hrEmpName"/>
						</td>
						<td class="TD_STYLE2">
						<!-- 单击此链接进入审核查询页面 -->
							<a href="/humanresources/move/detailByIdMove.action?id=${hrRemId }">审核</a>
						</td>
					</tr>
				</s:iterator>
			</table>
			<div class="pager">
			</div>
</form>
	</body>
</html>
