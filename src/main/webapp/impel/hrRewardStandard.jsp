<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>部门管理</title>
         <link rel="stylesheet" href="../css/table.css" type="text/css"></link>
         <script type="text/javascript">
         
          function toAdd(){
          document.getElementById("addUserInfo").style.display="block";
         }
         function none(){
          document.getElementById("addUserInfo").style.display="none";
         }
         function tiao(){
       
         var ye=document.getElementById("ye").value;
          if(isNaN(ye)){
         ye=1;
         }
        window.location.href="listhrRewardStandard.action?fenYe.nowPage="+ye;
         }
         
    function cha(){
        var name=document.getElementById("name").value;
        var td=document.getElementById("td");
        if(name.length==0){
           td.innerHTML="激励标准名称不能为空";
           return false;
        } else{
        td.innerHTML="";
        return true;
        }
     
 }
   function cha1(){
        var name=document.getElementById("one").value;
        var td=document.getElementById("td1");
        if(name.length==0){
           td.innerHTML="激励金额不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
      function cha2(){
        var name=document.getElementById("two").value;
        var td=document.getElementById("td2");
        if(name.length==0){
           td.innerHTML="结束时间不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha3(){
        var name=document.getElementById("three").value;
        var td=document.getElementById("td3");
        if(name.length==0){
           td.innerHTML="金额不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha4(){
        var name=document.getElementById("four").value;
        var td=document.getElementById("td4");
        if(name.length==0){
           td.innerHTML="备注不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
         function sub(){
          if(cha()&&cha1()){
          return true;
          }else{
          return false;
          }
         }
         </script>
  <style type="text/css">
   #addUserInfo {
	position:absolute;
	left:200px;
	top:58px;
	width:450px;
	height:200px;
	display:none;
	border: 1px solid #96E1A0;
}
span{
color:red;
}

         </style>
  </head>

	<body>
	<div id="addUserInfo">
	<form action="addhrRewardStandards.action" method="post" onsubmit="return sub()">
  <table width="450" height="200" border="0" style="background-color:#CAFBCA;">
    <tr>
      <td height="10" colspan="2" align="center" class="TD_STYLE1">添加激励标准</td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >激励标准的名称：</td>
      <td width="68%" >
      <input name="rs.hrStaName" id="name" type="text" onblur="cha()"/>
      <span id="td"></span>
      </td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >激励标准的金额：</td>
      <td width="68%" >
        <input name="rs.hrStaMoney" id="one" type="text" onblur="cha1()"/>
        <span id="td1"></span>
      </td>
    </tr>
     <tr>
      <td height="30" colspan="2" align="center" >
        <input type="submit" value="增加"/>	 	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit4" value="取消" onClick="none()"/>
      </td>
    </tr>
  </table>
  </form>
</div>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源管理--激励管理--激励标准管理</font>
						</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="button" value="添加" 
							onclick="toAdd();">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				bordercolorlight=#848284 bordercolordark=#eeeeee
				class="TABLE_STYLE1">
				<tr>
					<td  class="TD_STYLE1">
						激励标准编号
					</td>
					<td class="TD_STYLE1">
						激励标准的名称
					</td>
					<td class="TD_STYLE1">
				               激励标准的金额
					  </td>
					<td class="TD_STYLE1"> 
					操作
					</td>
					
</tr>
					<c:forEach items="${arr}" var="ma" varStatus="i">
					<tr>
						<td class="TD_STYLE2">
						${i.index+1}
						</td>
						<td class="TD_STYLE2">
						${ma.hrStaName}
						</td>
						<td class="TD_STYLE2">
						${ma.hrStaMoney}
						</td>
						<td   class="TD_STYLE2">
						<a href="chahrRewardStandard.action?id=${ma.hrStaId}">修改</a>
						<a href="deletehrRewardStandards.action?id=${ma.hrStaId}">删除</a>
						</td>
						
						
					</tr>
					</c:forEach>
			</table>
			<p>&nbsp;&nbsp;总数：${fenYe.totalNums}例 &nbsp;&nbsp;&nbsp;当前第 ${fenYe.nowPage } 页  &nbsp;&nbsp;&nbsp;共 ${fenYe.totalPages} 页  &nbsp;&nbsp;&nbsp;跳到第 <input id="ye" type=text class=input1 size=1> 页&nbsp;&nbsp;<img src="../images/go.bmp"  onclick="tiao()" />
	</body>
</html>