<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>用户管理</title>
         <link rel="stylesheet" href="../css/table.css" type="text/css"></link>
          <script src="jquery-1.8.0.js" type="text/javascript"></script>
          
         <script type="text/javascript">
         
          function toAdd(){
          document.getElementById("addUserInfo").style.display="block";
         }
         function none(){
          document.getElementById("addUserInfo").style.display="none";
         }
         function tiao(){
       
         var ye=document.getElementById("ye").value;
          if(isNaN(ye)){
         ye=1;
         }
          window.location.href="listtrainpersonnel.action?fenYe.nowPage="+ye;
         }
         function cha1(){
        var name=document.getElementById("name").value;
        var td=document.getElementById("td1");
        if(name.length==0){
           td.innerHTML="真实姓名不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
      function cha2(){
        var name=document.getElementById("username").value;
        var td=document.getElementById("td2");
        if(name.length==0){
           td.innerHTML="用户名不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha3(){
        var name=document.getElementById("password").value;
        var td=document.getElementById("td3");
        if(name.length==0){
           td.innerHTML="密码不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha4(){
        var name=document.getElementById("gree").value;
        var td=document.getElementById("td4");
        if(name.length==0){
           td.innerHTML="角色不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
         function sub(){
          if(cha1()&&cha2()&&cha3()&&cha4()){
          return true;
          }else{
          return false;
          }
         }
        
         $(document).ready(function(){
        	$("#ren").change(function(){
        		$.post("/humanresources/json/like.action",{"id": "1"},function(data){
        			alert("111");
        		},"json");
        	});
         });
       
         </script>
         <style type="text/css">
         span{
         color:red;
         }
   #addUserInfo {
	position:absolute;
	left:200px;
	top:58px;
	width:400px;
	height:240px;
	display:none;
	border: 1px solid #96E1A0;
}
         </style>
  </head>
	<body>
	<div id="addUserInfo">
	<form action="addtrainpersonnel.action" method="post" onsubmit="return  sub()">
  <table width="400" height="240" border="0" style="background-color:#CAFBCA;">
    <tr>
      <td height="10" colspan="2" align="center" class="TD_STYLE1">添加培训记录</td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >培训计划名称：</td>
      <td width="68%" >
       <select name="id">
       <c:forEach items="${trains}" var="tr">
       <option value="${tr.hrTraId}">${tr.hrTraName}</option>
       </c:forEach>
       </select>
      </td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >培训人员：</td>
      <td width="68%" >
        <select name="empid">
         <c:forEach items="${employees}" var="em">
         <option value="${em.hrEmpId}">${em.hrEmpName}</option>
        </c:forEach>
        </select>
      </td>
    </tr>
       <tr>
      <td width="32%" height="30" align="center" >培训成绩：</td>
      <td width="68%" >
        <input name="personnel.hrPerDegree" id="password" onblur="cha3()"  type="text"/><span id="td3"></span>
      </td>
    </tr>
      
       <tr>
      <td width="32%" height="30" align="center" >备注：</td>
      <td width="68%">
        <input name="personnel.hrPerRemark" id="gree" onblur="cha4()" type="text"/><span id="td4"></span>
      </td>
    </tr>
	<tr>
      <td height="30" colspan="2" align="center">
        <input type="submit" value="增加"/>	 	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit4" value="取消" onClick="none()"/>
      </td>
    </tr>
  </table>
  </form>
</div>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--培训管理--培训计划设置
						</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="button" value="添加" 
							onclick="toAdd();">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				bordercolorlight=#848284 bordercolordark=#eeeeee
				class="TABLE_STYLE1">
				<tr>
					<td  class="TD_STYLE1">
					培训计划编号
					</td>
					<td  class="TD_STYLE1">
					 培训项目名称
					</td>
					<td class="TD_STYLE1">
					培训员工
					</td>
				  <td class="TD_STYLE1">
					培训成绩
					</td>
					<td  class="TD_STYLE1" width="20%">
					     备注
					</td >
					   
					<td width="20%"   class="TD_STYLE1" >
					 操作
					</td>
					
</tr>
					<c:forEach items="${arr}" var="de" varStatus="i">
					<tr>
						<td class="TD_STYLE2">
						${i.index+1}
						</td>
						<td class="TD_STYLE2">
							${de.hrTrain.hrTraName}
						</td>
								<td class="TD_STYLE2">
							${de.hrEmployee.hrEmpName}
						</td>
								<td class="TD_STYLE2">
							${de.hrPerDegree}
						</td>
						
						<td class="TD_STYLE2">
						${de.hrPerRemark}
						</td>
						<td>
						<a href="deletetrainpersonnel.action?id=${de.hrPerId}">删除</a>
						</td>
						
					</tr>
					</c:forEach>
				
					 
				
			</table>
			<p>&nbsp;&nbsp;总数：${fenYe.totalNums}例 &nbsp;&nbsp;&nbsp;当前第 ${fenYe.nowPage } 页  &nbsp;&nbsp;&nbsp;共 ${fenYe.totalPages} 页  &nbsp;&nbsp;&nbsp;跳到第 <input id="ye" type=text class=input1 size=1> 页&nbsp;&nbsp;<img src="../images/go.bmp"  onclick="tiao()" />
	</body>
</html>
