<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<link rel="stylesheet" href="../css/table.css" type="text/css">
		<script type="text/javascript" src="../javascript/comm/comm.js"></script>
	</head>

	<body>
		<form action="job_administer_issueAction.do?methodName=Selectengage_Major_Release" method="post">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--职位发布管理--职位发布变更
						</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<br/>
						<br/>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				
				class="TABLE_STYLE1">
				<tr>
					<td width="15%" class="TD_STYLE1">职位名称</td>
					<td width="15%" class="TD_STYLE1">招聘类型</td>
					<td width="10%" class="TD_STYLE1">招聘人数</td>
					<td width="15%" class="TD_STYLE1">发布时间</td>
					<td width="15%" class="TD_STYLE1">截至时间</td>
				
					
				</tr>
					<s:iterator value="#request.selectMajorPut">
						<tr>
							<td class="TD_STYLE2">
								<s:property value="hrPutName"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutType"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutNumber"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutBeginDate"/>
							</td>
							<td class="TD_STYLE2">
							<!-- <fmt:formatDate var="ye" pattern="yyyy-MM-dd" value="${emr.deadline}"/> -->
								<s:property value="hrPutEndDate"/>
							</td>
							
							
						</tr>
					</s:iterator>
			</table>
			<p>&nbsp;&nbsp;总数：1例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="../../../images/go.bmp" width=18 height=18 border=0>
		</form>
	</body>
	<script type="text/javascript">
		function page_to(current){
			document.forms[0].currentPage.value=current;
			document.forms[0].submit();
		}
		function page_go(){
			document.forms[0].submit();
		}
	</script>
</html>