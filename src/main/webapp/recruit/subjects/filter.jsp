<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 试题查询成功页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css" />
		<!-- 导入日期控件 -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
	</head>

	<body>
	<!-- form提交到试题查询页面 -->
		<form method="post" action="/humanresources/exam/termExam.action">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源--招聘管理--招聘考试题库管理--试题查询</font>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<input type="submit" value="开始" class="BUTTON_STYLE1">
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>
					<td width="20%" class="TD_STYLE1">
						请选择试题分类
					</td>
					<td class="TD_STYLE2">														
						<select name="queryProperties.hrKindName" multiple="multiple" onclick="" style="width: 290;height: 100">								
								<s:iterator value="#request.questionsKind">
									<option value="${hrKindId }"><s:property value="hrKindName"/></option>
								</s:iterator>
						</select>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						请输入登记时间
					</td>
					
					<td class="TD_STYLE2">
						<input type="text" name="queryProperties.hrQueTime" value="" class="INPUT_STYLE1" onClick="WdatePicker()" id="nowTime">
						至
						<input type="text" name="queryProperties.endTime" value="" class="INPUT_STYLE1" onClick="WdatePicker()" id="nowTime">
						(YYYY-MM-DD)
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>