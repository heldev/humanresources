<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 试题添加页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css">
		<!-- 导入日期控件js -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		
	</head>
	
	<body>
	<!-- form提交到添加试题方法action -->
		<form action="/humanresources/exam/addQuestions.action" method="post">
		
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--招聘考试题库管理--试题登记 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="提交" class="BUTTON_STYLE1">
						<input type="reset" value="清除" class="BUTTON_STYLE1">
					</td>
				</tr>
			</table>		
			
			<table class="TABLE_STYLE1" border="1" width="100%" cellpadding=0>
				
				<tr class="TR_STYLE1">
					<td width="8%" nowrap class="TD_STYLE1">
						试题分类
					</td>
					<td width="14%">
						<select name="question.hrQuestionsKind.hrKindId"  class="SELECT_STYLE1">
							<option value="">--请选择--</option>						
							<s:iterator value="#request.questionsKind">
								<option value="${hrKindId}"><s:property value="hrKindName"/></option>
							</s:iterator>			
						</select>
					</td>
					
					
					<td width="8%" class="TD_STYLE1">
						登记时间
					</td>
					<td width="14%" class="TD_STYLE2">
						<input type="text" name="question.hrQueTime" onClick="WdatePicker()" class="INPUT_STYLE2 Wdate" id="nowTime"/>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						题干
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea 
							name="question.hrQueContent" class="TEXTAREA_STYLE1"
							rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						答案a
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea 
							name="question.hrQueKeyA" class="TEXTAREA_STYLE1"
							rows="4"></textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						答案b
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea 
							name="question.hrQueKeyB" class="TEXTAREA_STYLE1"
							rows="4" ></textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						答案c
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea 
							name="question.hrQueKeyC" class="TEXTAREA_STYLE1"
							rows="4" ></textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						答案d
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea 
							name="question.hrQueKeyD" class="TEXTAREA_STYLE1"
							rows="4" ></textarea>
					</td>
				</tr>

				<tr>
					<td class="TD_STYLE1">
						正确答案
					</td>
					<td class="TD_STYLE2">
						<input type="text" name="question.hrQueKeyCorrect" class="INPUT_STYLE2">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

