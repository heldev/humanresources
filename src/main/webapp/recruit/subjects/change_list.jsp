<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 试题变更成功页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>

		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css">
		
	</head>

	<body>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--招聘考试题库管理--试题变更 </font>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<input type="button" value="返回" class="BUTTON_STYLE1" onClick="history.back();">
						</div>
					</td>
				</tr>
			</table>

			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr class="TR_STYLE1">
					<td width="13%" class="TD_STYLE1">
						题干
					</td>
					<td width="16%" class="TD_STYLE1">
						登记日期
					</td>
					<td width="8%" class="TD_STYLE1">
						正确答案
					</td>
					<td width="8%" class="TD_STYLE1">
						变更
					</td>
				</tr>
			   <s:iterator value="#request.question">
				<tr class="TR_STYLE1">
					<td class="TD_STYLE2">
						<s:property value="hrQueContent"/>
					</td>
					<td class="TD_STYLE2">
						<s:property value="hrQueTime"/>
					</td>
					<td class="TD_STYLE2">
						<s:property value="hrQueKeyCorrect"/>
					</td>
					<td class="TD_STYLE2">
					<!-- 根据此链接进入试题修改页面 -->
						<a href="/humanresources/exam/examByIdUpdate.action?id=${hrQueId}">变更</a>
					</td>
				</tr>
			   </s:iterator>
			</table>
			<p>&nbsp;&nbsp;总数：1例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="../../../images/go.bmp" width=18 height=18 border=0>
	</body>
</html>
