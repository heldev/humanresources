<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 面试登记页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">

		<!-- 导入外部table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css">
		<!-- 导入jquery.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
		<!-- 导入validator.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.js"></script>
		<!-- 导入metadate.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.metadata.js"></script>
		<!-- 导入messages_cn.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.messages_cn.js" charset="utf-8"></script>
		<!-- 导入register.js -->
		<script type="text/javascript" src="/humanresources/javascript/recruit/resume/register.js"></script>
		
	</head>

	<body>
			
	<!-- form提交到添加面试action -->
		<form action="/humanresources/interview/addInterView.action" method="post">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--简历登记 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="提交" class=BUTTON_STYLE1>
						<input type="reset" value="清除" class="BUTTON_STYLE1">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>
					<td class="TD_STYLE1" width="11%">姓名</td>
					<td width="14%" class="TD_STYLE2"><input type="text" name="resume.hrPerName" value="${resume.hrPerName}" class="INPUT_STYLE2 required"></td>
					<td width="11%" class="TD_STYLE1">年龄</td>
					<td width="14%" class="TD_STYLE2"><input type="text" name="resume.hrPerAge" value="${resume.hrPerAge}" class="INPUT_STYLE2 required number digits"></td>
					<td width="11%" class="TD_STYLE1">申请职位</td>
					<td class="TD_STYLE2" colspan="2"><input type="text" name="resume.hrApplyMajor" value="${resume.hrApplyMajor }" class="INPUT_STYLE2 required"></td>
					
					
				</tr>
				
				<tr>
				  <td class="TD_STYLE1"> 性别 </td>
				  <td class="TD_STYLE2"><s:property value="#request.resume.hrPerSex"/></td>
					<td class="TD_STYLE1">手机</td>
					<td class="TD_STYLE2"><input type="text" name="resume.hrPerPhone" value="${resume.hrPerPhone }" class="INPUT_STYLE2 required"></td>
					<td class="TD_STYLE1">
						EMAIL
					</td>
					<td colspan="2" class="TD_STYLE2">
						<input type="text" name="resume.hrPerEmail" value="${resume.hrPerEmail }" class="INPUT_STYLE2 required email">
					</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td class="TD_STYLE1">籍贯</td>
					<td class="TD_STYLE2"><input type="text" name="resume.hrPerNative" value="${resume.hrPerNative }" class="INPUT_STYLE2 required"></td>
					<td class="TD_STYLE1">生日 </td>
					<td class="TD_STYLE2">
						<input type="text"  name="resume.hrPerBirthday" value="${ resume.hrPerBirthday}" class="INPUT_STYLE2 required dateISO">
					</td>
					<td class="TD_STYLE1" >身份证号码 </td>
					<td width="13%" class="TD_STYLE2" colspan="2">
						<input type="text" name="resume.hrPerCard" class="INPUT_STYLE2  required number" value="${resume.hrPerCard}" id="date_start">
					</td>
				</tr>
				
				<tr>
				  <td class="TD_STYLE1"> 学历 </td>
				  <td class="TD_STYLE2"><input type="text" name="resume.hrPerDegree" value="${ resume.hrPerDegree}" class="INPUT_STYLE2 required"></td>
					<td class="TD_STYLE1">专业</td>
					<td class="TD_STYLE2"><input type="text" name="resume.hrPerEducated" value="${resume.hrPerEducated }" class="INPUT_STYLE2 required"></td>
					<td class="TD_STYLE1">薪酬要求 </td>
					<td class="TD_STYLE2" colspan="2"><input type="text" value="${resume.hrPerSalaryNeed }" name="resume.hrPerSalaryNeed" class="INPUT_STYLE2 required number"></td>
				</tr>
				<tr>
					
				</tr>
				<tr>
					<td class="TD_STYLE1">工作经验</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="resume.hrPerJobRemark" rows="4" class="TEXTAREA_STYLE1 required"><s:property value="#request.resume.hrPerJobRemark"/> </textarea>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						个人履历</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="resume.hrPerResumeRemark" rows="4" class="TEXTAREA_STYLE1 required"><s:property value="#request.resume.hrPerResumeRemark"/></textarea>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				
				
				<tr>
					
					<td width="10%" class="TD_STYLE1">
						形象评价
					</td>
					<td width="15%" class="TD_STYLE2">
						<select name="interView.hrInteImageDegree" class="SELECT_STYLE1">
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
						</select>
					</td>
					<td width="10%" class="TD_STYLE1">应变能力</td>
					<td width="20%" class="TD_STYLE2">
						<select name="interView.hrInteResponseSpeedDegree" class="SELECT_STYLE1">
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
						</select>
					</td>
					
				</tr>
				<tr>
					<td class="TD_STYLE1">面试时间</td>
					<td class="TD_STYLE2"><input type="text" name="interView.hrInteRegisteTime" class="INPUT_STYLE2" id="nowTime2"/></td>
					<td class="TD_STYLE1">综合素质 </td>
					<td class="TD_STYLE1"><select name="interView.hrInteMultiQualityDegree" class="SELECT_STYLE1">
					  <option value="A">A</option>
					  <option value="B">B</option>
					  <option value="C">C</option>
					  </select></td>
					
				</tr>
				<tr>
					<td class="TD_STYLE1">
						面试评价
					</td>
					<td class="TD_STYLE2" colspan="7">
					<!-- 根据此信息进行对应的简历表添加 -->
					<input type="hidden" name="interView.hrResume.hrResId" value="${resume.hrResId }">
						<textarea
							name="interView.hrInteAppraise" class="TEXTAREA_STYLE1"
							rows="4"></textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>