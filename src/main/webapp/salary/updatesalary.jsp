<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>用户管理</title>
         <link rel="stylesheet" href="../css/table.css" type="text/css"></link>
         <script type="text/javascript">
         
          function toAdd(){
          document.getElementById("addUserInfo").style.display="block";
         }
         function none(){
          document.getElementById("addUserInfo").style.display="none";
         }
         function tiao(){
       
         var ye=document.getElementById("ye").value;
          window.location.href="listuser.action?fenYe.nowPage="+ye;
         }
         function cha1(){
        var name=document.getElementById("name").value;
        var td=document.getElementById("td1");
        if(name.length==0){
           td.innerHTML="薪酬标准名称不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
      function cha2(){
        var name=document.getElementById("username").value;
        var td=document.getElementById("td2");
        if(name.length==0){
           td.innerHTML="薪酬总金额不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha3(){
        var name=document.getElementById("password").value;
        var td=document.getElementById("td3");
        if(name.length==0){
           td.innerHTML="基本工资不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha4(){
        var name=document.getElementById("gree").value;
        var td=document.getElementById("td4");
        if(name.length==0){
           td.innerHTML="出差补助不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
            function cha5(){
        var name=document.getElementById("jiao").value;
        var td=document.getElementById("td5");
        if(name.length==0){
           td.innerHTML="交通补贴不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
            function cha6(){
        var name=document.getElementById("tong").value;
        var td=document.getElementById("td6");
        if(name.length==0){
           td.innerHTML="通讯补贴不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
            function cha7(){
        var name=document.getElementById("food").value;
        var td=document.getElementById("td7");
        if(name.length==0){
           td.innerHTML="午餐补贴不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
         function sub(){
          if(cha1()&&cha2()&&cha3()&&cha4()&&cha5()&&cha6()&&cha7()){
          return true;
          }else{
          return false;
          }
         }
         </script>
         <style type="text/css">
         span{
         color:red;
         }
         </style>
  </head>
  

	
		

	<body>
	<div id="addUserInfo">
	
</div>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--薪酬标准管理--薪酬标准设置
						</font>
					</td>
				</tr>
			</table>
			<form action="updatesalarys.action" method="post" onsubmit="return  sub()">
      <table width="100%" height="400" border="0" style="background-color:#CAFBCA;">
     <tr>
      <td height="10" colspan="2" align="center" class="TD_STYLE1">修改酬薪标准</td>
    </tr>
    <tr>
    <td width="32%" height="30" align="center" >酬薪标准编号：</td>
    <td  width="68%">
    ${salary.hrSalId}
    </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >酬薪标准名称：</td>
      <td width="68%" >
        <input name="salary.hrSalName" value="${salary.hrSalName}" id="name" type="text" onblur="cha1()"/><span id="td1"></span>
      </td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >薪酬总金额：</td>
      <td width="68%" >
        <input name="salary.hrSalRental" value="${salary.hrSalRental}" id="username" onblur="cha2()" type="text"/><span id="td2"></span>
      </td>
    </tr>
       <tr>
      <td width="32%" height="30" align="center" >基本工资：</td>
      <td width="68%" >
        <input name="salary.hrSalBase" value="${salary.hrSalBase}" id="password" onblur="cha3()"  type="text"/><span id="td3"></span>
      </td>
    </tr>
      
       <tr>
      <td width="32%" height="30" align="center" >出差补贴：</td>
      <td width="68%">
        <input name="salary.hrSalEvection" value="${salary.hrSalEvection}" id="gree" onblur="cha4()" type="text"/><span id="td4"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >交通补贴：</td>
      <td width="68%">
        <input name="salary.hrSalTraffic" value="${salary.hrSalTraffic}" id="jiao" onblur="cha5()" type="text"/><span id="td5"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >通讯补贴：</td>
      <td width="68%">
        <input name="salary.hrSalMessage" value="${salary.hrSalMessage}" id="tong" onblur="cha6()" type="text"/><span id="td6"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >午餐补贴：</td>
      <td width="68%">
        <input name="salary.hrSalFood" value="${salary.hrSalFood}" id="food" onblur="cha7()" type="text"/><span id="td7"></span>
      </td>
    </tr>
   
	<tr >
	 
      <td   height="30" colspan="2" >
      
        <input type="submit" value="修改" style="margin-left: 300px"/>	 	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		
      </td>
      
    </tr>
    
  </table>
  </form>
	</body>
</html>