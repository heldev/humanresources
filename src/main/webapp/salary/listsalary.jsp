<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>用户管理</title>
         <link rel="stylesheet" href="../css/table.css" type="text/css"></link>
         <script type="text/javascript">
         
          function toAdd(){
          document.getElementById("addUserInfo").style.display="block";
         }
         function none(){
          document.getElementById("addUserInfo").style.display="none";
         }
         function tiao(){
       
         var ye=document.getElementById("ye").value;
         if(isNaN(ye)){
         ye=1;
         }
          window.location.href="listsalary.action?fenYe.nowPage="+ye;
         }
         function cha1(){
        var name=document.getElementById("name").value;
        var td=document.getElementById("td1");
        if(name.length==0){
           td.innerHTML="薪酬标准名称不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
      function cha2(){
        var name=document.getElementById("username").value;
        var td=document.getElementById("td2");
        if(name.length==0){
           td.innerHTML="薪酬总金额不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha3(){
        var name=document.getElementById("password").value;
        var td=document.getElementById("td3");
        if(name.length==0){
           td.innerHTML="基本工资不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha4(){
        var name=document.getElementById("gree").value;
        var td=document.getElementById("td4");
        if(name.length==0){
           td.innerHTML="出差补助不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
            function cha5(){
        var name=document.getElementById("jiao").value;
        var td=document.getElementById("td5");
        if(name.length==0){
           td.innerHTML="交通补贴不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
            function cha6(){
        var name=document.getElementById("tong").value;
        var td=document.getElementById("td6");
        if(name.length==0){
           td.innerHTML="通讯补贴不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
            function cha7(){
        var name=document.getElementById("food").value;
        var td=document.getElementById("td7");
        if(name.length==0){
           td.innerHTML="午餐补贴不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
         function sub(){
          if(cha1()&&cha2()&&cha3()&&cha4()&&cha5()&&cha6()&&cha7()){
          return true;
          }else{
          return false;
          }
         }
         </script>
         <style type="text/css">
         span{
         color:red;
         }
   #addUserInfo {
	position:absolute;
	left:200px;
	top:58px;
	width:430px;
	height:240px;
	display:none;
	border: 1px solid #96E1A0;
}
         </style>
  </head>
  

	
		

	<body>
	<div id="addUserInfo">
	<form action="addsalarys.action" method="post" onsubmit="return  sub()">
  <table width="430" height="240" border="0" style="background-color:#CAFBCA;">
    <tr>
      <td height="10" colspan="2" align="center" class="TD_STYLE1">添加酬薪标准</td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >酬薪标准名称：</td>
      <td width="68%" >
        <input name="salary.hrSalName" id="name" type="text" onblur="cha1()"/><span id="td1"></span>
      </td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >薪酬总金额：</td>
      <td width="68%" >
        <input name="salary.hrSalRental" id="username" onblur="cha2()" type="text"/><span id="td2"></span>
      </td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >职位：</td>
      <td width="68%" >
       <select name="id">
      <c:forEach items="${list}" var="he">
        <option value="${he.hrMajId}">${he.hrMajName}</option>
        </c:forEach>
          </select>
      </td>
    </tr>
       <tr>
      <td width="32%" height="30" align="center" >基本工资：</td>
      <td width="68%" >
        <input name="salary.hrSalBase" id="password" onblur="cha3()"  type="text"/><span id="td3"></span>
      </td>
    </tr>
      
       <tr>
      <td width="32%" height="30" align="center" >出差补贴：</td>
      <td width="68%">
        <input name="salary.hrSalEvection" id="gree" onblur="cha4()" type="text"/><span id="td4"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >交通补贴：</td>
      <td width="68%">
        <input name="salary.hrSalTraffic" id="jiao" onblur="cha5()" type="text"/><span id="td5"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >通讯补贴：</td>
      <td width="68%">
        <input name="salary.hrSalMessage" id="tong" onblur="cha6()" type="text"/><span id="td6"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >午餐补贴：</td>
      <td width="68%">
        <input name="salary.hrSalFood" id="food" onblur="cha7()" type="text"/><span id="td7"></span>
      </td>
    </tr>
	<tr>
      <td height="30" colspan="2" align="center">
        <input type="submit" value="增加"/>	 	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit4" value="取消" onClick="none()"/>
      </td>
    </tr>
  </table>
  </form>
</div>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--薪酬标准管理--薪酬标准设置
						</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="button" value="添加" 
							onclick="toAdd();">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				bordercolorlight=#848284 bordercolordark=#eeeeee
				class="TABLE_STYLE1">
				<tr>
					<td  class="TD_STYLE1">
						薪酬编号
					</td>
					<td  class="TD_STYLE1">
						薪酬总金额
					</td>
					<td class="TD_STYLE1">
					    薪酬标准名称
					</td>
				  <td class="TD_STYLE1">
					基本工资
					</td>
					<td  class="TD_STYLE1">
					     出差补贴
					</td >
					<td class="TD_STYLE1">
					交通补贴
					</td>
						<td class="TD_STYLE1">
					通讯补贴
					</td>
						<td class="TD_STYLE1">
					午餐补贴
					</td>
					<td class="TD_STYLE1">
					制定时间
					</td>
					   
					<td width="20%"   class="TD_STYLE1" >
					 操作
					</td>
					
</tr>
					<c:forEach items="${arr}" var="de" varStatus="i">
					<tr>
						<td class="TD_STYLE2">
						${i.index+1}
						</td>
						<td class="TD_STYLE2">
							${de.hrSalRental}
						</td>
						<td class="TD_STYLE2">
							${de.hrSalName}
						</td>
						
						<td class="TD_STYLE2">
						${de.hrSalBase}
						</td>
						<td class="TD_STYLE2">
						${de.hrSalEvection}
						</td>
						<td class="TD_STYLE2">
						${de.hrSalTraffic}
						</td>
						<td class="TD_STYLE2">
						${de.hrSalMessage}
						</td>
						<td class="TD_STYLE2">
						${de.hrSalFood}
						</td>
						<td>
						${de.hrSalTime}
						</td>
						<td>
						<a href="deletesalarys.action?id=${de.hrSalId}">删除</a>
						<a href="dansalary.action?id=${de.hrSalId}" target="mainFrame">修改</a>
						</td>
					</tr>
					</c:forEach>

				
					
				
			</table>
			<p>&nbsp;&nbsp;总数：${fenYe.totalNums}例 &nbsp;&nbsp;&nbsp;当前第 ${fenYe.nowPage } 页  &nbsp;&nbsp;&nbsp;共 ${fenYe.totalPages} 页  &nbsp;&nbsp;&nbsp;跳到第 <input id="ye" type=text class=input1  size=1> 页&nbsp;&nbsp;<img src="../images/go.bmp"  onclick="tiao()" />
	</body>
</html>