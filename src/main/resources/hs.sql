/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.5.27 : Database - test2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `test2`;

/*Table structure for table `hr_city` */

DROP TABLE IF EXISTS `hr_city`;

CREATE TABLE `hr_city` (
  `HR_CITY_ID` int(11) NOT NULL,
  `HR_PRO_ID` int(11) DEFAULT NULL,
  `HR_CITY_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_CITY_ID`),
  KEY `FK72085780B9646504` (`HR_PRO_ID`),
  CONSTRAINT `FK72085780B9646504` FOREIGN KEY (`HR_PRO_ID`) REFERENCES `hr_province` (`HR_PRO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_city` */

/*Table structure for table `hr_department` */

DROP TABLE IF EXISTS `hr_department`;

CREATE TABLE `hr_department` (
  `HR_DEP_ID` int(11) NOT NULL,
  `HR_DEP_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_DEP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_department` */

insert  into `hr_department`(`HR_DEP_ID`,`HR_DEP_NAME`) values (1,'???');

/*Table structure for table `hr_employee` */

DROP TABLE IF EXISTS `hr_employee`;

CREATE TABLE `hr_employee` (
  `HR_EMP_ID` int(11) NOT NULL,
  `HR_SAL_ID` int(11) DEFAULT NULL,
  `HR_MAJ_ID` int(11) DEFAULT NULL,
  `HR_DEP_ID` int(11) DEFAULT NULL,
  `HR_EMP_NAME` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_SEX` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_AGE` int(11) DEFAULT NULL,
  `HR_EMP_EMAIL` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_QQ` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_BIRTHDAY` datetime DEFAULT NULL,
  `HR_EMP_DEGREE` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_EDUCATED` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_CARD` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_NATIVE` longtext COLLATE utf8_bin,
  `HR_EMP_FOLK` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_GOVERNMENT` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_RELIGION` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_MARRIAGE` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_JOB_DATER` datetime DEFAULT NULL,
  `HR_EMP_PHONE` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_ADDRESS` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_POSTCODE` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_OPEN_BANK` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_SPECIALITY` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_HOBBY` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_REGISTER_MAN` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_RESUME` longtext COLLATE utf8_bin,
  `HR_EMP_REMARKS` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_EMP_REGIST_TIME` datetime DEFAULT NULL,
  `HR_EMP_CHECK_STATUS` int(11) DEFAULT NULL,
  `HR_EMP_FILE_STATUS` int(11) DEFAULT NULL,
  `HR_EMP_JOB_STATUS` int(11) DEFAULT NULL,
  `HR_EMP_PHOTO` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_EMP_ID`),
  KEY `FK9B068A43F9DFEF60` (`HR_MAJ_ID`),
  KEY `FK9B068A433336BD4D` (`HR_SAL_ID`),
  KEY `FK9B068A43D8B09C04` (`HR_DEP_ID`),
  CONSTRAINT `FK9B068A433336BD4D` FOREIGN KEY (`HR_SAL_ID`) REFERENCES `hr_salary` (`HR_SAL_ID`),
  CONSTRAINT `FK9B068A43D8B09C04` FOREIGN KEY (`HR_DEP_ID`) REFERENCES `hr_department` (`HR_DEP_ID`),
  CONSTRAINT `FK9B068A43F9DFEF60` FOREIGN KEY (`HR_MAJ_ID`) REFERENCES `hr_major` (`HR_MAJ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_employee` */

insert  into `hr_employee`(`HR_EMP_ID`,`HR_SAL_ID`,`HR_MAJ_ID`,`HR_DEP_ID`,`HR_EMP_NAME`,`HR_EMP_SEX`,`HR_EMP_AGE`,`HR_EMP_EMAIL`,`HR_EMP_QQ`,`HR_EMP_BIRTHDAY`,`HR_EMP_DEGREE`,`HR_EMP_EDUCATED`,`HR_EMP_CARD`,`HR_EMP_NATIVE`,`HR_EMP_FOLK`,`HR_EMP_GOVERNMENT`,`HR_EMP_RELIGION`,`HR_EMP_MARRIAGE`,`HR_EMP_JOB_DATER`,`HR_EMP_PHONE`,`HR_EMP_ADDRESS`,`HR_EMP_POSTCODE`,`HR_EMP_OPEN_BANK`,`HR_EMP_SPECIALITY`,`HR_EMP_HOBBY`,`HR_EMP_REGISTER_MAN`,`HR_EMP_RESUME`,`HR_EMP_REMARKS`,`HR_EMP_REGIST_TIME`,`HR_EMP_CHECK_STATUS`,`HR_EMP_FILE_STATUS`,`HR_EMP_JOB_STATUS`,`HR_EMP_PHOTO`) values (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `hr_interview` */

DROP TABLE IF EXISTS `hr_interview`;

CREATE TABLE `hr_interview` (
  `HR_INTE_ID` int(11) NOT NULL,
  `HR_RES_ID` int(11) DEFAULT NULL,
  `HR_INTE_IMAGE_DEGREE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_INTE_RESPONSE_SPEED_DEGREE` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `HR_INTE_MULTI_QUALITY_DEGREE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_INTE_REGISTE_TIME` datetime DEFAULT NULL,
  `HR_INTE_APPRAISE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_INTE_STATUS` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_INTE_ID`),
  KEY `FK468DA06C30446FAE` (`HR_RES_ID`),
  CONSTRAINT `FK468DA06C30446FAE` FOREIGN KEY (`HR_RES_ID`) REFERENCES `hr_resume` (`HR_RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_interview` */

/*Table structure for table `hr_major` */

DROP TABLE IF EXISTS `hr_major`;

CREATE TABLE `hr_major` (
  `HR_MAJ_ID` int(11) NOT NULL,
  `HR_DEP_ID` int(11) DEFAULT NULL,
  `HR_MAJ_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_MAJ_ID`),
  KEY `FKCF8BBA24D8B09C04` (`HR_DEP_ID`),
  CONSTRAINT `FKCF8BBA24D8B09C04` FOREIGN KEY (`HR_DEP_ID`) REFERENCES `hr_department` (`HR_DEP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_major` */

/*Table structure for table `hr_major_put` */

DROP TABLE IF EXISTS `hr_major_put`;

CREATE TABLE `hr_major_put` (
  `HR_PUT_ID` int(11) NOT NULL,
  `HR_PUT_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `HR_PUT_NUMBER` int(11) DEFAULT NULL,
  `HR_PUT_TYPE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_PUT_BEGIN_DATE` datetime DEFAULT NULL,
  `HR_PUT_END_DATE` datetime DEFAULT NULL,
  `HR_PUT_REQUIRE` longtext COLLATE utf8_bin,
  `HR_PUT_STATUS` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_PUT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_major_put` */

/*Table structure for table `hr_province` */

DROP TABLE IF EXISTS `hr_province`;

CREATE TABLE `hr_province` (
  `HR_PRO_ID` int(11) NOT NULL,
  `HR_PRO_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_PRO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_province` */

/*Table structure for table `hr_questions` */

DROP TABLE IF EXISTS `hr_questions`;

CREATE TABLE `hr_questions` (
  `HR_QUE_ID` int(11) NOT NULL,
  `HR_KIND_ID` int(11) DEFAULT NULL,
  `HR_QUE_TIME` datetime DEFAULT NULL,
  `HR_QUE_CONTENT` longtext COLLATE utf8_bin,
  `HR_QUE_KEY_A` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_QUE_KEY_B` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_QUE_KEY_C` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_QUE_KEY_D` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_QUE_KEY_CORRECT` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_QUE_ID`),
  KEY `FKBE561038D86F8020` (`HR_KIND_ID`),
  CONSTRAINT `FKBE561038D86F8020` FOREIGN KEY (`HR_KIND_ID`) REFERENCES `hr_questions_kind` (`HR_KIND_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_questions` */

/*Table structure for table `hr_questions_kind` */

DROP TABLE IF EXISTS `hr_questions_kind`;

CREATE TABLE `hr_questions_kind` (
  `HR_KIND_ID` int(11) NOT NULL,
  `HR_KIND_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_KIND_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_questions_kind` */

/*Table structure for table `hr_remove` */

DROP TABLE IF EXISTS `hr_remove`;

CREATE TABLE `hr_remove` (
  `HR_REM_ID` int(11) NOT NULL,
  `HR_DEP_NEW_ID` int(11) DEFAULT NULL,
  `HR_MAJ_NEW_ID` int(11) DEFAULT NULL,
  `HR_EMP_ID` int(11) DEFAULT NULL,
  `HR_DEP_OLD_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_MAJ_OLD_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_REM_CAUSE` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_REM_TIME` datetime DEFAULT NULL,
  `HR_REM_STATUS` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_REM_REGISTER_MAN` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_REM_REGISTER_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`HR_REM_ID`),
  KEY `FK2AAD81592857D087` (`HR_EMP_ID`),
  KEY `FK2AAD81595D196883` (`HR_DEP_NEW_ID`),
  KEY `FK2AAD81594564675F` (`HR_MAJ_NEW_ID`),
  CONSTRAINT `FK2AAD81592857D087` FOREIGN KEY (`HR_EMP_ID`) REFERENCES `hr_employee` (`HR_EMP_ID`),
  CONSTRAINT `FK2AAD81594564675F` FOREIGN KEY (`HR_MAJ_NEW_ID`) REFERENCES `hr_major` (`HR_MAJ_ID`),
  CONSTRAINT `FK2AAD81595D196883` FOREIGN KEY (`HR_DEP_NEW_ID`) REFERENCES `hr_department` (`HR_DEP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_remove` */

/*Table structure for table `hr_resume` */

DROP TABLE IF EXISTS `hr_resume`;

CREATE TABLE `hr_resume` (
  `HR_RES_ID` int(11) NOT NULL,
  `HR_PER_NAME` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_APPLY_MAJOR` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_SEX` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_PHONE` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_EMAIL` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_NATIVE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_BIRTHDAY` datetime DEFAULT NULL,
  `HR_PER_CARD` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_DEGREE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_EDUCATED` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_SALARY_NEED` double DEFAULT NULL,
  `HR_PER_JOB_REMARK` longtext COLLATE utf8_bin,
  `HR_PER_RESUME_REMARK` longtext COLLATE utf8_bin,
  `HR_PER_PHOTO` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_AGE` int(11) DEFAULT NULL,
  `HR_PER_STATUS` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_resume` */

/*Table structure for table `hr_reward` */

DROP TABLE IF EXISTS `hr_reward`;

CREATE TABLE `hr_reward` (
  `HR_REW_ID` int(11) NOT NULL,
  `HR_EMP_ID` int(11) DEFAULT NULL,
  `HR_STA_ID` int(11) DEFAULT NULL,
  `HR_REW_TIME` datetime DEFAULT NULL,
  `HR_APPROVE_LEADER` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_REW_ID`),
  KEY `FK2AB1D8042857D087` (`HR_EMP_ID`),
  KEY `FK2AB1D804F805330D` (`HR_STA_ID`),
  CONSTRAINT `FK2AB1D8042857D087` FOREIGN KEY (`HR_EMP_ID`) REFERENCES `hr_employee` (`HR_EMP_ID`),
  CONSTRAINT `FK2AB1D804F805330D` FOREIGN KEY (`HR_STA_ID`) REFERENCES `hr_reward_standard` (`HR_STA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_reward` */

/*Table structure for table `hr_reward_standard` */

DROP TABLE IF EXISTS `hr_reward_standard`;

CREATE TABLE `hr_reward_standard` (
  `HR_STA_ID` int(11) NOT NULL,
  `HR_STA_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_STA_MONEY` double DEFAULT NULL,
  PRIMARY KEY (`HR_STA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_reward_standard` */

/*Table structure for table `hr_salary` */

DROP TABLE IF EXISTS `hr_salary`;

CREATE TABLE `hr_salary` (
  `HR_SAL_ID` int(11) NOT NULL,
  `HR_MAJ_ID` int(11) DEFAULT NULL,
  `HR_SAL_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_SAL_RENTAL` double DEFAULT NULL,
  `HR_SAL_REGISTER` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_SAL_TIME` datetime DEFAULT NULL,
  `HR_SAL_REMARK` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_SAL_BASE` double DEFAULT NULL,
  `HR_SAL_EVECTION` double DEFAULT NULL,
  `HR_SAL_TRAFFIC` double DEFAULT NULL,
  `HR_SAL_MESSAGE` double DEFAULT NULL,
  `HR_SAL_FOOD` double DEFAULT NULL,
  PRIMARY KEY (`HR_SAL_ID`),
  KEY `FK2C29529FF9DFEF60` (`HR_MAJ_ID`),
  CONSTRAINT `FK2C29529FF9DFEF60` FOREIGN KEY (`HR_MAJ_ID`) REFERENCES `hr_major` (`HR_MAJ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_salary` */

/*Table structure for table `hr_train` */

DROP TABLE IF EXISTS `hr_train`;

CREATE TABLE `hr_train` (
  `HR_TRA_ID` int(11) NOT NULL,
  `HR_TRA_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `HR_TRA_BEGIN_DATE` datetime DEFAULT NULL,
  `HR_TRA_END_DATE` datetime DEFAULT NULL,
  `HR_TRA_MONEY` double DEFAULT NULL,
  `HR_TRA_DESC` longtext COLLATE utf8_bin,
  PRIMARY KEY (`HR_TRA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_train` */

/*Table structure for table `hr_train_personnel` */

DROP TABLE IF EXISTS `hr_train_personnel`;

CREATE TABLE `hr_train_personnel` (
  `HR_PER_ID` int(11) NOT NULL,
  `HR_TRA_ID` int(11) DEFAULT NULL,
  `HR_EMP_ID` int(11) DEFAULT NULL,
  `HR_PER_DEGREE` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_PER_REMARK` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_PER_ID`),
  KEY `FK486A9F342857D087` (`HR_EMP_ID`),
  KEY `FK486A9F347279042` (`HR_TRA_ID`),
  CONSTRAINT `FK486A9F342857D087` FOREIGN KEY (`HR_EMP_ID`) REFERENCES `hr_employee` (`HR_EMP_ID`),
  CONSTRAINT `FK486A9F347279042` FOREIGN KEY (`HR_TRA_ID`) REFERENCES `hr_train` (`HR_TRA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_train_personnel` */

/*Table structure for table `hr_user` */

DROP TABLE IF EXISTS `hr_user`;

CREATE TABLE `hr_user` (
  `HR_USER_ID` int(11) NOT NULL,
  `HR_EMP_ID` int(11) DEFAULT NULL,
  `HR_USER_PWD` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `HR_USER_NAME` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `HR_USER_DEGREE` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`HR_USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `hr_user` */

insert  into `hr_user`(`HR_USER_ID`,`HR_EMP_ID`,`HR_USER_PWD`,`HR_USER_NAME`,`HR_USER_DEGREE`) values (1,3,'123123','123','123123');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
