package com.rs.impel.action;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrReward;
import com.rs.common.entity.HrRewardStandard;
import com.rs.common.entity.HrUser;
import com.rs.files.biz.EmployeeBiz;
import com.rs.impel.biz.HrRewardBiz;
import com.rs.impel.biz.HrRewardStandardBiz;

public class HrRewardAction extends ActionSupport {
	private HrRewardBiz hrRewardBiz;
	private HrRewardStandardBiz hrRewardStandardBiz;
	private Integer id;
	private List<HrReward> list;
	private List<HrRewardStandard> arr;
	private HrReward rd;
	private FenYe fenYe;
	private List<HrEmployee> employees;
	private EmployeeBiz employeeBiz;
	private Integer empid;

	public void setId(Integer id) {
		this.id = id;
	}

	public void setEmpid(Integer empid) {
		this.empid = empid;
	}

	public void setEmployeeBiz(EmployeeBiz employeeBiz) {
		this.employeeBiz = employeeBiz;
	}

	public List<HrEmployee> getEmployees() {
		return employees;
	}

	public FenYe getFenYe() {
		return fenYe;
	}

	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}

	public HrReward getRd() {
		return rd;
	}

	public void setRd(HrReward rd) {
		this.rd = rd;
	}

	public List<HrReward> getList() {
		return list;
	}

	public List<HrRewardStandard> getArr() {
		return arr;
	}

	public void setHrRewardStandardBiz(HrRewardStandardBiz hrRewardStandardBiz) {
		this.hrRewardStandardBiz = hrRewardStandardBiz;
	}

	public void setHrRewardBiz(HrRewardBiz hrRewardBiz) {
		this.hrRewardBiz = hrRewardBiz;
	}

	public String add() {
		try {
			Map<String, Object> session = ActionContext.getContext()
					.getSession();
			HrUser user = (HrUser) session.get("user");
			HrEmployee em = employeeBiz.getHrEmployeeById(empid);
			HrRewardStandard rs = hrRewardStandardBiz
					.getHrRewardStandardByid(id);
			rd.setHrEmployee(em);
			rd.setHrRewardStandard(rs);
			hrRewardBiz.addHrReward(rd);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}

	public String delete() {
		try {
			hrRewardBiz.deleteHrReward(id);
			return SUCCESS;
		} catch (Exception e) {

			e.printStackTrace();
			return ERROR;
		}
	}

	public String list() {
		try {
			employees = employeeBiz.getHrEmployeeBydepartment();
			arr = hrRewardStandardBiz.getAllHrRewardStandard();
			list = hrRewardBiz.getHrReward(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
}
