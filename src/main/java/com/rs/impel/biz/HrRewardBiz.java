package com.rs.impel.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrReward;

public interface HrRewardBiz {

	//增加激励记录
	public   void addHrReward(HrReward rd);
	//删除激励记录
	public void  deleteHrReward(Integer id);
	//分页查询激励记录
	public List<HrReward> getHrReward(FenYe fenYe);
}
