package com.rs.impel.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrRewardStandard;

public interface HrRewardStandardBiz {

	//增加激励标准
	public void addHrRewardStandard(HrRewardStandard rs);
	//修改激励标准
	public void updateHrRewardStandard(HrRewardStandard rs);
	//单查激励标准
	public HrRewardStandard getHrRewardStandardByid(Integer id);
	//全查所有的激励标准
	public List<HrRewardStandard> getAllHrRewardStandard();
	//分页查询
	public  List<HrRewardStandard> getHrRewardStandard(FenYe fenYe);
	//删除激励标准
	public void deleteHrRewardStandard(Integer id);
}
