package com.rs.impel.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrReward;
import com.rs.impel.biz.HrRewardBiz;
import com.rs.impel.dao.HrRewardDao;

public class HrRewardBizImpl implements HrRewardBiz {

	private HrRewardDao hrRewardDao;
	
	public void setHrRewardDao(HrRewardDao hrRewardDao) {
		this.hrRewardDao = hrRewardDao;
	}
	//增加激励记录
	public void addHrReward(HrReward rd) {
		// TODO Auto-generated method stub
		hrRewardDao.addEntity(rd);
	}
  //删除激励记录
	public void deleteHrReward(Integer id) {
		// TODO Auto-generated method stub
		hrRewardDao.deleteEntityById(HrReward.class, id);
	}
 //分页查询
	public List<HrReward> getHrReward(FenYe fenYe) {
		// TODO Auto-generated method stub
		return hrRewardDao.getList(HrReward.class, fenYe);
	}

}
