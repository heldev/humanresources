package com.rs.impel.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrRewardStandard;

public interface HrRewardStandardDao extends  CommonDao{

	//全查所有的激励标准
	public List<HrRewardStandard> getAllHrRewardStandard();
}
