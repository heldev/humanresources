package com.rs.recruit.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.HrMajorPut;
import com.rs.recruit.biz.MajorPutBiz;

/**
 * 本模块为职位发布管理模块
 * @author 贾志新
 * 修改时间 2013-11-11
 */
public class MajorPutAction extends ActionSupport implements RequestAware {

	//调用biz
	private MajorPutBiz majorPutBiz;
	//entity实体进行添加
	private HrMajorPut majorPut;
	//进行biz层的注入
	public void setMajorPutBiz(MajorPutBiz majorPutBiz) {
		this.majorPutBiz = majorPutBiz;
	}
	//进行ID修改先查询单个ID对象
	private Integer id;
	
	//让页面的ID进行注入
	public void setId(Integer id) {
		this.id = id;
	}
	//封装对象进行注入
	public HrMajorPut getMajorPut() {
		return majorPut;
	}
	//封装对象进行注入
	public void setMajorPut(HrMajorPut majorPut) {
		this.majorPut = majorPut;
	}
	//进行公用request设置
	Map<String,Object> request;
	public void setRequest(Map<String, Object> request) {
		this.request=request;
	}
	//添加职位发放信息
	public String addMajorPut(){
		/*
		 * 写try catch 目的
		 * 如果返回SUCCESS跳转到成功页面
		 * 如果返回INPUT 跳转到失败页面
		 */
		try {
			// 进行职位添加方法
			majorPutBiz.addMajorPut(majorPut);
			return SUCCESS;
		} catch (Exception e) {
			// 进行异常处理
			e.printStackTrace();
		}
		return ERROR;
	}
	//查询所有职位发放查询
	public String selectMajorPut(){
		try {
			/**
			 * 如果查询成功的话进入查询页面
			 * 查询失败进入查询失败页面
			 */
			List<HrMajorPut> selectMajorPut = majorPutBiz.selectAll();
			request.put("selectMajorPut", selectMajorPut);
			return SUCCESS;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据ID删除职位发布信息
	public String deleMajor(){
		/*
		 * 此处写try catch 目的
		 * 如果返回为SUCCESS跳转到删除成功页面
		 * 如果返回为INPUT跳转到删除失败页面
		 */
		try {
			majorPutBiz.delMajor(id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 单击修改按钮先查询单个ID对象
	public String modifyChangingMajorPutSelect(){
		try {
			/**
			 * 此处写try catch目的如果id为空的话进行异常处理
			 */
			//查询单个ID对象
			HrMajorPut majorPut=majorPutBiz.getMajorPutById(id); //把查询的信息放到对象中
			//准备把查询信息放到集合中
			request.put("majorPut", majorPut);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 职位信息修改方法
	public String updateMajorPut(){
		/*
		 * 此处try catch目的
		 * 如果返回SUCCESS跳到修改成功页面
		 * 如果返回INPUT跳转到修改失败页面
		 */
		try {
			majorPutBiz.updateMajorPut(majorPut);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	// 查询所有职位信息方法
	public String queryMajor(){
		try {
			List<HrMajorPut> selectMajorPut = majorPutBiz.selectAll();
			request.put("selectMajorPut", selectMajorPut);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
}

