package com.rs.recruit.dao;

import java.util.List;

import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrMajorPut;

public interface MajorPutDao {

	//查询职位表的所有职位名称
	public List<HrMajor> selectMajName();
	
	//进行职位信息添加
	public void addMajorPut(HrMajorPut majorPut);
	
	//查询职位表发放所有信息
	public List<HrMajorPut> selectAll();
	
	//进行职位表信息修改
	public void updateMajorPut(HrMajorPut majorPut);
	
	//根据ID查询单个职位信息
	public HrMajorPut getMajorPutById(Integer hrPutId);
	
	//根据ID删除该条职位信息
	public void delMajor(Integer id);
}
