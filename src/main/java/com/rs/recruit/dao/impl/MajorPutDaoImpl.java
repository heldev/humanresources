package com.rs.recruit.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrMajorPut;
import com.rs.recruit.dao.MajorPutDao;

public class MajorPutDaoImpl extends HibernateDaoSupport implements MajorPutDao {

	//查询所有职位名称
	public List<HrMajor> selectMajName() {
		return (List<HrMajor>)getHibernateTemplate().find("select hrMajName from HrMajor");
	}
	//进行职位信息添加实现
	public void addMajorPut(HrMajorPut majorPut) {
		getHibernateTemplate().save(majorPut);
	}
	//进行职位所有信息查询
	public List<HrMajorPut> selectAll() {
		return (List<HrMajorPut>)getHibernateTemplate().find("from HrMajorPut");
	}
	//进行修改所有职位信息查询
	public void updateMajorPut(HrMajorPut majorPut) {
		getHibernateTemplate().update(majorPut);
	}
	//进行单个信息查询
	public HrMajorPut getMajorPutById(Integer hrPutId) {
		return getHibernateTemplate().get(HrMajorPut.class, hrPutId);
	}
	//进行ID删除Major信息
	public void delMajor(Integer id) {
		//根据ID查询当个对象进行删除
		HrMajorPut majorPut=getMajorPutById(id);
		getHibernateTemplate().delete(majorPut); //进行持久化删除
	}
}
