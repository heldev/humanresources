package com.rs.recruit.biz.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrMajorPut;
import com.rs.recruit.biz.MajorPutBiz;
import com.rs.recruit.dao.MajorPutDao;

public class MajorPutBizImpl implements MajorPutBiz {

	//根据spring进行Dao注入
	private MajorPutDao majorPutDao;
	
	//封装set方法，进行依赖注入
	public void setMajorPutDao(MajorPutDao majorPutDao) {
		this.majorPutDao = majorPutDao;
	}

	//调用Dao层的查询职位方法
	public List<HrMajor> selectMajName() {
		return majorPutDao.selectMajName();
	}

	//调用Dao层的添加方法
	public void addMajorPut(HrMajorPut majorPut) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = majorPut.getHrPutBeginDate();
		
		majorPutDao.addMajorPut(majorPut);
	}
	
	//调用Dao层所有查询方法
	public List<HrMajorPut> selectAll() {
		return majorPutDao.selectAll();
	}
	
	//调用Dao层根据ID查询职位对象
	public HrMajorPut getMajorPutById(Integer hrPutId) {
		return majorPutDao.getMajorPutById(hrPutId);
	}
	
	//调用Dao层修改职位信息
	public void updateMajorPut(HrMajorPut majorPut) {
		majorPutDao.updateMajorPut(majorPut);
	}
	
	//调用Dao层删除职位信息
	public void delMajor(Integer id) {
		majorPutDao.delMajor(id);
	}
}
