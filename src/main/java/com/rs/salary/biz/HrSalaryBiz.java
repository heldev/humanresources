package com.rs.salary.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrSalary;
/*
 * 作者:王运发
 * 时间:2013年11月6日 17:14:44
 */
public interface HrSalaryBiz {

	//增加工资标准
	void  addHrSalary(HrSalary sa);
	//删除工资标准
	void deleteHrSalary(Integer id);
	//修改工资标准
	void updateHrSalary(HrSalary sa);
	//分页查询
	 List<HrSalary>  getHrSalary(FenYe fenYe);
	//全查
	 List<HrSalary>  getAllSalary();
	 //单查工资标准
	 HrSalary  getSalaryById(Integer id);

}
