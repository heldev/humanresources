package com.rs.salary.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrSalary;

/*
 * 作者:王运发
 * 时间:2013年11月6日 17:14:44
 */
public interface HrSalaryDao extends CommonDao{

	//查询所有的工资标准
	public List<HrSalary> getAllSalary();
	
}
