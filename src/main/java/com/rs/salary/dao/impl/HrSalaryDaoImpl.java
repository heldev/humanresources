package com.rs.salary.dao.impl;

import java.io.Serializable;
import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrSalary;
import com.rs.salary.dao.HrSalaryDao;

/*
 * 作者:王运发
 * 时间:2013年11月6日 17:14:44
 */
public class HrSalaryDaoImpl extends CommonDaoImpl implements HrSalaryDao {

	//查询所有的工资
	public List<HrSalary> getAllSalary() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from HrSalary");
	}



}
