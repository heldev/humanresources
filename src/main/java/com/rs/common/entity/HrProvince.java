package com.rs.common.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * HrProvince entity. @author MyEclipse Persistence Tools
 */

public class HrProvince implements java.io.Serializable {

	// Fields

	private Integer hrProId;
	private String hrProName;
	private Set hrCities = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrProvince() {
	}

	/** full constructor */
	public HrProvince(String hrProName, Set hrCities) {
		this.hrProName = hrProName;
		this.hrCities = hrCities;
	}

	// Property accessors

	public Integer getHrProId() {
		return this.hrProId;
	}

	public void setHrProId(Integer hrProId) {
		this.hrProId = hrProId;
	}

	public String getHrProName() {
		return this.hrProName;
	}

	public void setHrProName(String hrProName) {
		this.hrProName = hrProName;
	}

	public Set getHrCities() {
		return this.hrCities;
	}

	public void setHrCities(Set hrCities) {
		this.hrCities = hrCities;
	}

}