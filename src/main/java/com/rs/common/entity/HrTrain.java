package com.rs.common.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * HrTrain entity. @author MyEclipse Persistence Tools
 */

public class HrTrain implements java.io.Serializable {

	// Fields

	private Integer hrTraId;
	private String hrTraName;
	private Date hrTraBeginDate;
	private Date hrTraEndDate;
	private Double hrTraMoney;
	private String hrTraDesc;
	private Set hrTrainPersonnels = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrTrain() {
	}

	/** full constructor */
	public HrTrain(String hrTraName, Date hrTraBeginDate, Date hrTraEndDate,
			Double hrTraMoney, String hrTraDesc, Set hrTrainPersonnels) {
		this.hrTraName = hrTraName;
		this.hrTraBeginDate = hrTraBeginDate;
		this.hrTraEndDate = hrTraEndDate;
		this.hrTraMoney = hrTraMoney;
		this.hrTraDesc = hrTraDesc;
		this.hrTrainPersonnels = hrTrainPersonnels;
	}

	// Property accessors

	public Integer getHrTraId() {
		return this.hrTraId;
	}

	public void setHrTraId(Integer hrTraId) {
		this.hrTraId = hrTraId;
	}

	public String getHrTraName() {
		return this.hrTraName;
	}

	public void setHrTraName(String hrTraName) {
		this.hrTraName = hrTraName;
	}

	public Date getHrTraBeginDate() {
		return this.hrTraBeginDate;
	}

	public void setHrTraBeginDate(Date hrTraBeginDate) {
		this.hrTraBeginDate = hrTraBeginDate;
	}

	public Date getHrTraEndDate() {
		return this.hrTraEndDate;
	}

	public void setHrTraEndDate(Date hrTraEndDate) {
		this.hrTraEndDate = hrTraEndDate;
	}

	public Double getHrTraMoney() {
		return this.hrTraMoney;
	}

	public void setHrTraMoney(Double hrTraMoney) {
		this.hrTraMoney = hrTraMoney;
	}

	public String getHrTraDesc() {
		return this.hrTraDesc;
	}

	public void setHrTraDesc(String hrTraDesc) {
		this.hrTraDesc = hrTraDesc;
	}

	public Set getHrTrainPersonnels() {
		return this.hrTrainPersonnels;
	}

	public void setHrTrainPersonnels(Set hrTrainPersonnels) {
		this.hrTrainPersonnels = hrTrainPersonnels;
	}

}