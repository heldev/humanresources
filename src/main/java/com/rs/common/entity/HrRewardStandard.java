package com.rs.common.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * HrRewardStandard entity. @author MyEclipse Persistence Tools
 */

public class HrRewardStandard implements java.io.Serializable {

	// Fields

	private Integer hrStaId;
	private String hrStaName;
	private Double hrStaMoney;
	private Set hrRewards = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrRewardStandard() {
	}

	/** full constructor */
	public HrRewardStandard(String hrStaName, Double hrStaMoney, Set hrRewards) {
		this.hrStaName = hrStaName;
		this.hrStaMoney = hrStaMoney;
		this.hrRewards = hrRewards;
	}

	// Property accessors

	public Integer getHrStaId() {
		return this.hrStaId;
	}

	public void setHrStaId(Integer hrStaId) {
		this.hrStaId = hrStaId;
	}

	public String getHrStaName() {
		return this.hrStaName;
	}

	public void setHrStaName(String hrStaName) {
		this.hrStaName = hrStaName;
	}

	public Double getHrStaMoney() {
		return this.hrStaMoney;
	}

	public void setHrStaMoney(Double hrStaMoney) {
		this.hrStaMoney = hrStaMoney;
	}

	public Set getHrRewards() {
		return this.hrRewards;
	}

	public void setHrRewards(Set hrRewards) {
		this.hrRewards = hrRewards;
	}

}