package com.rs.common.entity;

import java.util.Date;

/**
 * HrMajorPut entity. @author MyEclipse Persistence Tools
 */

public class HrMajorPut implements java.io.Serializable {

	// Fields

	private Integer hrPutId;
	private String hrPutName;
	private Integer hrPutNumber;
	private String hrPutType;
	private Date hrPutBeginDate;
	private Date hrPutEndDate;
	private String hrPutRequire;
	private String hrPutStatus;

	// Constructors

	/** default constructor */
	public HrMajorPut() {
	}

	/** full constructor */
	public HrMajorPut(String hrPutName, Integer hrPutNumber, String hrPutType,
			Date hrPutBeginDate, Date hrPutEndDate, String hrPutRequire,
			String hrPutStatus) {
		this.hrPutName = hrPutName;
		this.hrPutNumber = hrPutNumber;
		this.hrPutType = hrPutType;
		this.hrPutBeginDate = hrPutBeginDate;
		this.hrPutEndDate = hrPutEndDate;
		this.hrPutRequire = hrPutRequire;
		this.hrPutStatus = hrPutStatus;
	}

	// Property accessors

	public Integer getHrPutId() {
		return this.hrPutId;
	}

	public void setHrPutId(Integer hrPutId) {
		this.hrPutId = hrPutId;
	}

	public String getHrPutName() {
		return this.hrPutName;
	}

	public void setHrPutName(String hrPutName) {
		this.hrPutName = hrPutName;
	}

	public Integer getHrPutNumber() {
		return this.hrPutNumber;
	}

	public void setHrPutNumber(Integer hrPutNumber) {
		this.hrPutNumber = hrPutNumber;
	}

	public String getHrPutType() {
		return this.hrPutType;
	}

	public void setHrPutType(String hrPutType) {
		this.hrPutType = hrPutType;
	}

	public Date getHrPutBeginDate() {
		return this.hrPutBeginDate;
	}

	public void setHrPutBeginDate(Date hrPutBeginDate) {
		this.hrPutBeginDate = hrPutBeginDate;
	}

	public Date getHrPutEndDate() {
		return this.hrPutEndDate;
	}

	public void setHrPutEndDate(Date hrPutEndDate) {
		this.hrPutEndDate = hrPutEndDate;
	}

	public String getHrPutRequire() {
		return this.hrPutRequire;
	}

	public void setHrPutRequire(String hrPutRequire) {
		this.hrPutRequire = hrPutRequire;
	}

	public String getHrPutStatus() {
		return this.hrPutStatus;
	}

	public void setHrPutStatus(String hrPutStatus) {
		this.hrPutStatus = hrPutStatus;
	}

}