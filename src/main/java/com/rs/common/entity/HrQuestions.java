package com.rs.common.entity;

import java.util.Date;

/**
 * HrQuestions entity. @author MyEclipse Persistence Tools
 */

public class HrQuestions implements java.io.Serializable {

	// Fields

	private Integer hrQueId;
	private HrQuestionsKind hrQuestionsKind;
	private Date hrQueTime;
	private String hrQueContent;
	private String hrQueKeyA;
	private String hrQueKeyB;
	private String hrQueKeyC;
	private String hrQueKeyD;
	private String hrQueKeyCorrect;

	// Constructors

	/** default constructor */
	public HrQuestions() {
	}

	/** full constructor */
	public HrQuestions(HrQuestionsKind hrQuestionsKind, Date hrQueTime,
			String hrQueContent, String hrQueKeyA, String hrQueKeyB,
			String hrQueKeyC, String hrQueKeyD, String hrQueKeyCorrect) {
		this.hrQuestionsKind = hrQuestionsKind;
		this.hrQueTime = hrQueTime;
		this.hrQueContent = hrQueContent;
		this.hrQueKeyA = hrQueKeyA;
		this.hrQueKeyB = hrQueKeyB;
		this.hrQueKeyC = hrQueKeyC;
		this.hrQueKeyD = hrQueKeyD;
		this.hrQueKeyCorrect = hrQueKeyCorrect;
	}

	// Property accessors

	public Integer getHrQueId() {
		return this.hrQueId;
	}

	public void setHrQueId(Integer hrQueId) {
		this.hrQueId = hrQueId;
	}

	public HrQuestionsKind getHrQuestionsKind() {
		return this.hrQuestionsKind;
	}

	public void setHrQuestionsKind(HrQuestionsKind hrQuestionsKind) {
		this.hrQuestionsKind = hrQuestionsKind;
	}

	public Date getHrQueTime() {
		return this.hrQueTime;
	}

	public void setHrQueTime(Date hrQueTime) {
		this.hrQueTime = hrQueTime;
	}

	public String getHrQueContent() {
		return this.hrQueContent;
	}

	public void setHrQueContent(String hrQueContent) {
		this.hrQueContent = hrQueContent;
	}

	public String getHrQueKeyA() {
		return this.hrQueKeyA;
	}

	public void setHrQueKeyA(String hrQueKeyA) {
		this.hrQueKeyA = hrQueKeyA;
	}

	public String getHrQueKeyB() {
		return this.hrQueKeyB;
	}

	public void setHrQueKeyB(String hrQueKeyB) {
		this.hrQueKeyB = hrQueKeyB;
	}

	public String getHrQueKeyC() {
		return this.hrQueKeyC;
	}

	public void setHrQueKeyC(String hrQueKeyC) {
		this.hrQueKeyC = hrQueKeyC;
	}

	public String getHrQueKeyD() {
		return this.hrQueKeyD;
	}

	public void setHrQueKeyD(String hrQueKeyD) {
		this.hrQueKeyD = hrQueKeyD;
	}

	public String getHrQueKeyCorrect() {
		return this.hrQueKeyCorrect;
	}

	public void setHrQueKeyCorrect(String hrQueKeyCorrect) {
		this.hrQueKeyCorrect = hrQueKeyCorrect;
	}

}