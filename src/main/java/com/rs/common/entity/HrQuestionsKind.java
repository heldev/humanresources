package com.rs.common.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * HrQuestionsKind entity. @author MyEclipse Persistence Tools
 */

public class HrQuestionsKind implements java.io.Serializable {

	// Fields

	private Integer hrKindId;
	private String hrKindName;
	private Set hrQuestionses = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrQuestionsKind() {
	}

	/** full constructor */
	public HrQuestionsKind(String hrKindName, Set hrQuestionses) {
		this.hrKindName = hrKindName;
		this.hrQuestionses = hrQuestionses;
	}

	// Property accessors

	public Integer getHrKindId() {
		return this.hrKindId;
	}

	public void setHrKindId(Integer hrKindId) {
		this.hrKindId = hrKindId;
	}

	public String getHrKindName() {
		return this.hrKindName;
	}

	public void setHrKindName(String hrKindName) {
		this.hrKindName = hrKindName;
	}

	public Set getHrQuestionses() {
		return this.hrQuestionses;
	}

	public void setHrQuestionses(Set hrQuestionses) {
		this.hrQuestionses = hrQuestionses;
	}

}