package com.rs.common.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * HrResume entity. @author MyEclipse Persistence Tools
 */

public class HrResume implements java.io.Serializable {

	// Fields

	private Integer hrResId;
	private String hrPerName;
	private String hrApplyMajor;
	private String hrPerSex;
	private String hrPerPhone;
	private String hrPerEmail;
	private String hrPerNative;
	private Date hrPerBirthday;
	private String hrPerCard;
	private String hrPerDegree;
	private String hrPerEducated;
	private Double hrPerSalaryNeed;
	private String hrPerJobRemark;
	private String hrPerResumeRemark;
	private String hrPerPhoto;
	private Integer hrPerAge;
	private String hrPerStatus;
	private Set hrInterviews = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrResume() {
	}

	/** full constructor */
	public HrResume(String hrPerName, String hrApplyMajor, String hrPerSex,
			String hrPerPhone, String hrPerEmail, String hrPerNative,
			Date hrPerBirthday, String hrPerCard, String hrPerDegree,
			String hrPerEducated, Double hrPerSalaryNeed,
			String hrPerJobRemark, String hrPerResumeRemark, String hrPerPhoto,
			Integer hrPerAge, String hrPerStatus, Set hrInterviews) {
		this.hrPerName = hrPerName;
		this.hrApplyMajor = hrApplyMajor;
		this.hrPerSex = hrPerSex;
		this.hrPerPhone = hrPerPhone;
		this.hrPerEmail = hrPerEmail;
		this.hrPerNative = hrPerNative;
		this.hrPerBirthday = hrPerBirthday;
		this.hrPerCard = hrPerCard;
		this.hrPerDegree = hrPerDegree;
		this.hrPerEducated = hrPerEducated;
		this.hrPerSalaryNeed = hrPerSalaryNeed;
		this.hrPerJobRemark = hrPerJobRemark;
		this.hrPerResumeRemark = hrPerResumeRemark;
		this.hrPerPhoto = hrPerPhoto;
		this.hrPerAge = hrPerAge;
		this.hrPerStatus = hrPerStatus;
		this.hrInterviews = hrInterviews;
	}

	// Property accessors

	public Integer getHrResId() {
		return this.hrResId;
	}

	public void setHrResId(Integer hrResId) {
		this.hrResId = hrResId;
	}

	public String getHrPerName() {
		return this.hrPerName;
	}

	public void setHrPerName(String hrPerName) {
		this.hrPerName = hrPerName;
	}

	public String getHrApplyMajor() {
		return this.hrApplyMajor;
	}

	public void setHrApplyMajor(String hrApplyMajor) {
		this.hrApplyMajor = hrApplyMajor;
	}

	public String getHrPerSex() {
		return this.hrPerSex;
	}

	public void setHrPerSex(String hrPerSex) {
		this.hrPerSex = hrPerSex;
	}

	public String getHrPerPhone() {
		return this.hrPerPhone;
	}

	public void setHrPerPhone(String hrPerPhone) {
		this.hrPerPhone = hrPerPhone;
	}

	public String getHrPerEmail() {
		return this.hrPerEmail;
	}

	public void setHrPerEmail(String hrPerEmail) {
		this.hrPerEmail = hrPerEmail;
	}

	public String getHrPerNative() {
		return this.hrPerNative;
	}

	public void setHrPerNative(String hrPerNative) {
		this.hrPerNative = hrPerNative;
	}

	public Date getHrPerBirthday() {
		return this.hrPerBirthday;
	}

	public void setHrPerBirthday(Date hrPerBirthday) {
		this.hrPerBirthday = hrPerBirthday;
	}

	public String getHrPerCard() {
		return this.hrPerCard;
	}

	public void setHrPerCard(String hrPerCard) {
		this.hrPerCard = hrPerCard;
	}

	public String getHrPerDegree() {
		return this.hrPerDegree;
	}

	public void setHrPerDegree(String hrPerDegree) {
		this.hrPerDegree = hrPerDegree;
	}

	public String getHrPerEducated() {
		return this.hrPerEducated;
	}

	public void setHrPerEducated(String hrPerEducated) {
		this.hrPerEducated = hrPerEducated;
	}

	public Double getHrPerSalaryNeed() {
		return this.hrPerSalaryNeed;
	}

	public void setHrPerSalaryNeed(Double hrPerSalaryNeed) {
		this.hrPerSalaryNeed = hrPerSalaryNeed;
	}

	public String getHrPerJobRemark() {
		return this.hrPerJobRemark;
	}

	public void setHrPerJobRemark(String hrPerJobRemark) {
		this.hrPerJobRemark = hrPerJobRemark;
	}

	public String getHrPerResumeRemark() {
		return this.hrPerResumeRemark;
	}

	public void setHrPerResumeRemark(String hrPerResumeRemark) {
		this.hrPerResumeRemark = hrPerResumeRemark;
	}

	public String getHrPerPhoto() {
		return this.hrPerPhoto;
	}

	public void setHrPerPhoto(String hrPerPhoto) {
		this.hrPerPhoto = hrPerPhoto;
	}

	public Integer getHrPerAge() {
		return this.hrPerAge;
	}

	public void setHrPerAge(Integer hrPerAge) {
		this.hrPerAge = hrPerAge;
	}

	public String getHrPerStatus() {
		return this.hrPerStatus;
	}

	public void setHrPerStatus(String hrPerStatus) {
		this.hrPerStatus = hrPerStatus;
	}

	public Set getHrInterviews() {
		return this.hrInterviews;
	}

	public void setHrInterviews(Set hrInterviews) {
		this.hrInterviews = hrInterviews;
	}

}