package com.rs.common.entity;

import java.util.Date;

/**
 * HrReward entity. @author MyEclipse Persistence Tools
 */

public class HrReward implements java.io.Serializable {

	// Fields

	private Integer hrRewId;
	private HrEmployee hrEmployee;
	private HrRewardStandard hrRewardStandard;
	private Date hrRewTime;
	private String hrApproveLeader;

	// Constructors

	/** default constructor */
	public HrReward() {
	}

	/** full constructor */
	public HrReward(HrEmployee hrEmployee, HrRewardStandard hrRewardStandard,
			Date hrRewTime, String hrApproveLeader) {
		this.hrEmployee = hrEmployee;
		this.hrRewardStandard = hrRewardStandard;
		this.hrRewTime = hrRewTime;
		this.hrApproveLeader = hrApproveLeader;
	}

	// Property accessors

	public Integer getHrRewId() {
		return this.hrRewId;
	}

	public void setHrRewId(Integer hrRewId) {
		this.hrRewId = hrRewId;
	}

	public HrEmployee getHrEmployee() {
		return this.hrEmployee;
	}

	public void setHrEmployee(HrEmployee hrEmployee) {
		this.hrEmployee = hrEmployee;
	}

	public HrRewardStandard getHrRewardStandard() {
		return this.hrRewardStandard;
	}

	public void setHrRewardStandard(HrRewardStandard hrRewardStandard) {
		this.hrRewardStandard = hrRewardStandard;
	}

	public Date getHrRewTime() {
		return this.hrRewTime;
	}

	public void setHrRewTime(Date hrRewTime) {
		this.hrRewTime = hrRewTime;
	}

	public String getHrApproveLeader() {
		return this.hrApproveLeader;
	}

	public void setHrApproveLeader(String hrApproveLeader) {
		this.hrApproveLeader = hrApproveLeader;
	}

}