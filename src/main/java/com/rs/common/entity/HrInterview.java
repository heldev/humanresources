package com.rs.common.entity;

import java.util.Date;

/**
 * HrInterview entity. @author MyEclipse Persistence Tools
 */

public class HrInterview implements java.io.Serializable {

	// Fields

	private Integer hrInteId;
	private HrResume hrResume;
	private String hrInteImageDegree;
	private String hrInteResponseSpeedDegree;
	private String hrInteMultiQualityDegree;
	private Date hrInteRegisteTime;
	private String hrInteAppraise;
	private String hrInteStatus;

	// Constructors

	/** default constructor */
	public HrInterview() {
	}

	/** full constructor */
	public HrInterview(HrResume hrResume, String hrInteImageDegree,
			String hrInteResponseSpeedDegree, String hrInteMultiQualityDegree,
			Date hrInteRegisteTime, String hrInteAppraise, String hrInteStatus) {
		this.hrResume = hrResume;
		this.hrInteImageDegree = hrInteImageDegree;
		this.hrInteResponseSpeedDegree = hrInteResponseSpeedDegree;
		this.hrInteMultiQualityDegree = hrInteMultiQualityDegree;
		this.hrInteRegisteTime = hrInteRegisteTime;
		this.hrInteAppraise = hrInteAppraise;
		this.hrInteStatus = hrInteStatus;
	}

	// Property accessors

	public Integer getHrInteId() {
		return this.hrInteId;
	}

	public void setHrInteId(Integer hrInteId) {
		this.hrInteId = hrInteId;
	}

	public HrResume getHrResume() {
		return this.hrResume;
	}

	public void setHrResume(HrResume hrResume) {
		this.hrResume = hrResume;
	}

	public String getHrInteImageDegree() {
		return this.hrInteImageDegree;
	}

	public void setHrInteImageDegree(String hrInteImageDegree) {
		this.hrInteImageDegree = hrInteImageDegree;
	}

	public String getHrInteResponseSpeedDegree() {
		return this.hrInteResponseSpeedDegree;
	}

	public void setHrInteResponseSpeedDegree(String hrInteResponseSpeedDegree) {
		this.hrInteResponseSpeedDegree = hrInteResponseSpeedDegree;
	}

	public String getHrInteMultiQualityDegree() {
		return this.hrInteMultiQualityDegree;
	}

	public void setHrInteMultiQualityDegree(String hrInteMultiQualityDegree) {
		this.hrInteMultiQualityDegree = hrInteMultiQualityDegree;
	}

	public Date getHrInteRegisteTime() {
		return this.hrInteRegisteTime;
	}

	public void setHrInteRegisteTime(Date hrInteRegisteTime) {
		this.hrInteRegisteTime = hrInteRegisteTime;
	}

	public String getHrInteAppraise() {
		return this.hrInteAppraise;
	}

	public void setHrInteAppraise(String hrInteAppraise) {
		this.hrInteAppraise = hrInteAppraise;
	}

	public String getHrInteStatus() {
		return this.hrInteStatus;
	}

	public void setHrInteStatus(String hrInteStatus) {
		this.hrInteStatus = hrInteStatus;
	}

}