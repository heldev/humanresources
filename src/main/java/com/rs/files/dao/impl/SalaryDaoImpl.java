package com.rs.files.dao.impl;

import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrSalary;
import com.rs.files.dao.SalaryDao;

public class SalaryDaoImpl<T> extends CommonDaoImpl<T> implements SalaryDao<T> {

	public List<HrSalary> getByIdSalary(int sid) {
		return this.getHibernateTemplate().find("from HrSalary s where s.hrMajor.hrMajId=?",sid);
	}

	public HrSalary getByIdSal(int sid) {
		return (HrSalary) this.getHibernateTemplate().get(HrSalary.class, sid);
	}

}
