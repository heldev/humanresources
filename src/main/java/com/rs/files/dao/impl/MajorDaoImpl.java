package com.rs.files.dao.impl;

import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrMajor;
import com.rs.files.dao.MajorDao;

public class MajorDaoImpl<T> extends CommonDaoImpl<T> implements MajorDao<T> {

	public List<HrMajor> getAllMajor(int pid) {
		
		return this.getHibernateTemplate().find("from HrMajor m where m.hrDepartment.hrDepId=?", pid);
	}

	public HrMajor getByIdMaj(int mid) {
		
		return (HrMajor) this.getHibernateTemplate().get(HrMajor.class, mid);
	}

}
