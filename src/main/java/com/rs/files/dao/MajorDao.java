package com.rs.files.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrMajor;

public interface MajorDao<T> extends CommonDao<T>{

	public List<HrMajor> getAllMajor(int pid);
	public HrMajor getByIdMaj(int mid);
}
