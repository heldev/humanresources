package com.rs.files.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrDepartment;
/**
 * 
 * @author wanghao
 *
 * @param <T>
 */
public interface DepartmentDao<T> extends CommonDao<T> {
	//µÃµ½ËùÓÐ²¿ÃÅ
	public List<HrDepartment> getAllDepartment();
	public HrDepartment getByIdDep(int pid);
}
