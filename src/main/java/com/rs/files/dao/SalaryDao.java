package com.rs.files.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrSalary;

public interface SalaryDao<T> extends CommonDao<T> {

	public List<HrSalary> getByIdSalary(int sid);
	public HrSalary getByIdSal(int sid);
}
