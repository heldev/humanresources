package com.rs.files.biz.impl;

import java.util.List;

import com.rs.common.entity.HrDepartment;
import com.rs.files.biz.DepartmentBiz;
import com.rs.files.dao.DepartmentDao;

public class DepartmentBizImpl implements DepartmentBiz{

	private static final Class HrDepartment = null;
	private DepartmentDao departmentDao;
	
	public void setDepartmentDao(DepartmentDao departmentDao) {
		this.departmentDao = departmentDao;
	}

	public List<HrDepartment> getAllDepartment() {
		
		return departmentDao.getAllDepartment();
	}

	public HrDepartment getByIdDep(int pid) {
		
		return departmentDao.getByIdDep(pid);
	}

}
