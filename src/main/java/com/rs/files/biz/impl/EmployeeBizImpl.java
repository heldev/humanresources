package com.rs.files.biz.impl;

import java.util.List;

import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.QueryProperties;
import com.rs.files.biz.EmployeeBiz;
import com.rs.files.dao.EmployeeDao;

public class EmployeeBizImpl implements EmployeeBiz {

	private EmployeeDao employeeDao;

	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	public void addEmployee(HrEmployee emp) {
		employeeDao.addEntity(emp);
		
	}

	public List<HrEmployee> getPropEmp(QueryProperties prop) {
		
		return employeeDao.getPropEmp(prop);
	}

	public void updateEmployee(int id) {
		HrEmployee emp=(HrEmployee) employeeDao.getEntityById(HrEmployee.class, id);
		emp.setHrEmpJobStatus(0);
		emp.setHrEmpFileStatus(0);
		employeeDao.updateEntity(emp);
		
	}

	public List<HrEmployee> deleteFileEmp(int status) {
		
		return employeeDao.deleteFileEmp(status);
	}

	public List<HrEmployee> updateJobEmp(int status) {
		return employeeDao.updateJobEmp(status);
	}

	public void updatesEmployee(int id) {
		HrEmployee emp=(HrEmployee) employeeDao.getEntityById(HrEmployee.class, id);
		emp.setHrEmpJobStatus(1);
		emp.setHrEmpFileStatus(1);
		employeeDao.updateEntity(emp);
		
	}

	public void deleteEmp(int empId) {
		employeeDao.deleteEntityById(HrEmployee.class, empId);
		
	}

	public HrEmployee selEmployee(int empId) {
		
		return (HrEmployee) employeeDao.getEntityById(HrEmployee.class, empId);
	}

	public void upEmployee(HrEmployee emp) {
		employeeDao.updateEntity(emp);
		
	}

	//根据部门编号查询所有的员工
	public void setHrEmployeeDao(EmployeeDao hrEmployeeDao) {
		this.employeeDao = hrEmployeeDao;
	}

	public List<HrEmployee> getHrEmployeeBydepartment() {
		
	   return employeeDao.getHrEmployeeBydepartment();
	}

	
	public HrEmployee getHrEmployeeById(Integer id) {
		// TODO Auto-generated method stub
		return employeeDao.getentity(id);
	}
	// 调用Dao层实现所有员工
	public List<HrEmployee> selectEmployee() {
		return (List<HrEmployee>)employeeDao.selectEmployee();
	}
}
