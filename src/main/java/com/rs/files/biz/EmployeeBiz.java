package com.rs.files.biz;

import java.util.List;

import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.QueryProperties;

public interface EmployeeBiz{

	public void addEmployee(HrEmployee emp);
	public List<HrEmployee> getPropEmp(QueryProperties prop);
	public void updateEmployee(int id);
	public void updatesEmployee(int id);
	public List<HrEmployee> deleteFileEmp(int status);
	public List<HrEmployee> updateJobEmp(int status);
	public void deleteEmp(int empId);
	public HrEmployee selEmployee(int empId);
	public void upEmployee(HrEmployee emp);
	
	//根据部门编号查询所有的员工
	public List<HrEmployee> getHrEmployeeBydepartment() ;
	//单查用户
	public HrEmployee getHrEmployeeById(Integer id);
	 public List<HrEmployee> selectEmployee();
}
