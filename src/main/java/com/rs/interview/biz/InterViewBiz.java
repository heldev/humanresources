package com.rs.interview.biz;

import java.util.List;

import com.rs.common.entity.HrInterview;
import com.rs.common.entity.HrResume;

public interface InterViewBiz {

	// 简历查询方法定义
	public List<HrResume> listInterView();
	
	// 简历单查方法定义
	public HrResume getById(Integer id);
	
	// 定义添加方法
	public void addInterView(HrInterview interview);
	
	// 定义修改方法
	public void updateInterView(HrResume resume);
	
	// 定义面试查询方法
	public List<HrInterview> interviewList();
	
	// 定义面试修改方法
	public void upInterView(HrInterview view);
	
	// 定义根据id查询单个面试方法
	public HrInterview sInterviewById(Integer id);
	
	// 定义所有建议录用方法
	public List<HrInterview> selectEmploy();
}
