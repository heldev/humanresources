package com.rs.interview.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.rs.common.entity.HrInterview;
import com.rs.common.entity.HrResume;
import com.rs.interview.dao.InterViewDao;



public class InterViewDaoImpl extends HibernateDaoSupport implements InterViewDao {

	// 实现简历列表查询
	public List<HrResume> listInterView() {
		return (List<HrResume>) getHibernateTemplate().find("from HrResume hr where hr.hrPerStatus='审核通过'");
	}
	// 实现单查方法
	public HrResume getById(Integer id) {
		return getHibernateTemplate().get(HrResume.class, id);
	}
	// 实现添加方法
	public void addInterView(HrInterview interview) {
		getHibernateTemplate().save(interview);
	}
	// 实现修改方法
	public void updateInterView(HrResume resume) {
		getHibernateTemplate().update(resume);
	}
	// 实现面试查询方法
	public List<HrInterview> interviewList() {
		return (List<HrInterview>) getHibernateTemplate().find("from HrInterview hr where hr.hrInteStatus='审核通过'");
	}
	// 实现面试修改方法
	public void upInterView(HrInterview view) {
		getHibernateTemplate().update(view);
	}
	// 实现根据单个id查询面试信息
	public HrInterview sInterviewById(Integer id) {
		return getHibernateTemplate().get(HrInterview.class, id);
	}
	// 实现建议录用方法
	public List<HrInterview> selectEmploy() {
		return (List<HrInterview>)getHibernateTemplate().find("from HrInterview");
	}
}
