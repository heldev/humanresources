package com.rs.move.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrRemove;
import com.rs.common.entity.HrSalary;
import com.rs.move.biz.ReMoveBiz;
import com.rs.move.dao.DataTranObject;
import com.rs.move.dao.Dto;
import com.rs.move.dao.QueryProperties;
/**
 * 调动管理模块
 * @author 贾志新
 *
 */
public class ReMoveAction extends ActionSupport implements RequestAware {

	// 调用biz层进行注入
	private ReMoveBiz reMoveBiz;

	// 申明数据传输对象
	private List<DataTranObject> tranObject;

	// 申明薪水
	private Dto dto;
	
	// 根据id进行查询
	private Integer id;
	
	// 进行对象封装
	private QueryProperties qp;

	// 进行对象注入
	private HrRemove move;

	// 进行通用设置
	private Map<String, Object> request;

	
	// 进行get封装
	public QueryProperties getQp() {
		return qp;
	}

	public Dto getDto() {
		return dto;
	}

	public void setDto(Dto dto) {
		this.dto = dto;
	}

	// 让页面进行注入
	public void setQp(QueryProperties qp) {
		this.qp = qp;
	}
	// 进行get封装
	public HrRemove getMove() {
		return move;
	}

	// 进行set封装
	public void setMove(HrRemove move) {
		this.move = move;
	}

	// 进行get封装
	public List<DataTranObject> getTranObject() {
		return tranObject;
	}
	
	// 　进行set封装
	public void setTranObject(List<DataTranObject> tranObject) {
		this.tranObject = tranObject;
	}
	// 进行set注入
	public void setReMoveBiz(ReMoveBiz reMoveBiz) {
		this.reMoveBiz = reMoveBiz;
	}

	// 进行set封装
	public void setId(Integer id) {
		this.id = id;
	}
	
	// 通用方法
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}
	// 部门id
	private Integer dpId;
	
	// 进行set封装
	public void setDpId(Integer dpId) {
		this.dpId = dpId;
	}
	// 进行get封装
	public Integer getDpId() {
		return dpId;
	}

	// 查询方法
	public String selectMove() {
		List<HrRemove> move = reMoveBiz.removeList();
		request.put("move", move);
		return SUCCESS;
	}

	// 调动
	public String registerMove() {
		try {
			HrEmployee move = reMoveBiz.move(id);
			List<HrDepartment> department = reMoveBiz.selectDepartment();
			request.put("move", move);
			request.put("department", department);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}

	// 查询职位方法
	public String smajorMove() {
		// 此处不要忘记new 对象
		try {
			tranObject = new ArrayList<DataTranObject>();
			List<HrMajor> cs = reMoveBiz.selectMajor(dpId);
			for (int i = 0; i < cs.size(); i++) {
				DataTranObject d = new DataTranObject();
				d.setId(cs.get(i).getHrMajId());
				d.setName(cs.get(i).getHrMajName());
				tranObject.add(d);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	// 职位调动修改
	public String updateMove(){
		try {
			reMoveBiz.addRemove(move);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据条件进行职位审核
	public String selectRemoveMove(){
		try {
			List<HrRemove> remove=reMoveBiz.stayAuditing();
			request.put("remove", remove);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 查询所有部门
	public String register_locate(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果查询成功返回到成功页面
			 * 如果查询失败返回到查询失败页面
			 */
			List<HrDepartment> selectDepartment = reMoveBiz.selectDepartment();
			request.put("selectDepartment", selectDepartment);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据部门id查询职位信息
	public String majorGetById(){
		try {
			/**
			 * 根据id进行查询查询结果为SUCCESS返回成功页面
			 * 查询结果为ERROR返回根据id查询失败页面
			 */
			tranObject = new ArrayList<DataTranObject>();
			List<HrMajor> selectMajor = reMoveBiz.selectMajor(dpId);
			for (int i = 0; i < selectMajor.size(); i++) {
				DataTranObject d = new DataTranObject();
				d.setId(selectMajor.get(i).getHrMajId());
				d.setName(selectMajor.get(i).getHrMajName());
				tranObject.add(d);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 模糊查询
	public String blurQuery(){
		try {
			List<HrEmployee> blurQuery = reMoveBiz.blurQuery(qp);
			request.put("blurQuery", blurQuery);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 查询审核通过id
	public String detailByIdMove(){
		try {
			HrRemove remove = reMoveBiz.remove(id);
			
			List<HrSalary> selectSalary = reMoveBiz.selectSalary();
			request.put("remove", remove);
			request.put("selectSalary", selectSalary);
			
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 新部门查询
	public String checkMove(){
		try {
			/**
			 * 根据id进行查询查询结果为SUCCESS返回成功页面
			 * 查询结果为ERROR返回根据id查询失败页面
			 */
			tranObject = new ArrayList<DataTranObject>();
			List<HrMajor> selectMajor = reMoveBiz.selectMajor(dpId);
			for (int i = 0; i < selectMajor.size(); i++) {
				DataTranObject d = new DataTranObject();
				d.setId(selectMajor.get(i).getHrMajId());
				d.setName(selectMajor.get(i).getHrMajName());
				tranObject.add(d);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 薪水id查询
	public String newSalary(){
		try {
			HrSalary salary = reMoveBiz.getByIdSalary(dpId);
			dto = new Dto();
			dto.setHrSalRental(salary.getHrSalRental());
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// rd1查询
	public String rd1(){
		move.setHrRemStatus("审核通过");
		reMoveBiz.updateRemove(move);
		HrEmployee employee = reMoveBiz.employeeGetId(move.getHrEmployee().getHrEmpId());
		employee.getHrDepartment().setHrDepId(move.getHrDepartment().getHrDepId());
		employee.getHrMajor().setHrMajId(move.getHrMajor().getHrMajId());
		employee.getHrSalary().setHrSalId(move.getHrEmployee().getHrSalary().getHrSalId());
		reMoveBiz.updateEmployee(employee);
		return SUCCESS;
	}
	// rd2查询
	public String rd2(){
		try {
			HrRemove move = reMoveBiz.remove(dpId);
			move.setHrRemStatus("审核失败");
			reMoveBiz.updateRemove(move);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 调动查询部门
	public String moveRemoveMove(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果根据id查询成功，返回成功页面
			 * 如果查询失败，返回失败页面
			 */
			List<HrDepartment> department = reMoveBiz.selectDepartment();
			request.put("department", department);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据部门id查询职位
	public String slocate(){
		try {
			/**
			 * 根据id进行查询查询结果为SUCCESS返回成功页面
			 * 查询结果为ERROR返回根据id查询失败页面
			 */
			tranObject = new ArrayList<DataTranObject>();
			List<HrMajor> selectMajor = reMoveBiz.selectMajor(dpId);
			for (int i = 0; i < selectMajor.size(); i++) {
				DataTranObject d = new DataTranObject();
				d.setId(selectMajor.get(i).getHrMajId());
				d.setName(selectMajor.get(i).getHrMajName());
				tranObject.add(d);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 模糊查询
	public String moveSelect(){
		try {
			List<HrRemove> remove = reMoveBiz.sMove(qp);
			request.put("remove", remove);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据id返回部门调动详细页面
	public String detailMove(){
		try {
			HrRemove remove = reMoveBiz.remove(id);
			request.put("remove", remove);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
}
