package com.rs.move.biz.impl;

import java.util.List;

import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrRemove;
import com.rs.common.entity.HrSalary;
import com.rs.move.biz.ReMoveBiz;
import com.rs.move.dao.QueryProperties;
import com.rs.move.dao.ReMoveDao;

public class ReMoveBizImpl implements ReMoveBiz {

	// 根据spring注入进行注入
	private ReMoveDao reMoveDao;
	
	// 封装set方法
	public void setReMoveDao(ReMoveDao reMoveDao) {
		this.reMoveDao = reMoveDao;
	}

	// 调用Dao层实现调用查询方法
	public List<HrRemove> removeList() {
		return (List<HrRemove>)reMoveDao.removeList();
	}
	// 调用Dao层单个查询方法
	public HrEmployee move(Integer id) {
		return reMoveDao.move(id);
	}
	// 调用Dao层部门查询方法
	public List<HrDepartment> selectDepartment() {
		return (List<HrDepartment>)reMoveDao.selectDepartment();
	}
	// 调用Dao层职位查询方法
	public List<HrMajor> selectMajor(Integer id) {
		return (List<HrMajor>)reMoveDao.selectMajor(id);
	}
	// 调用Dao层实现职位方法修改
	public void updateRemove(HrRemove move) {
		reMoveDao.updateRemove(move);
	}
	// 调用Dao层实现添加职位方法
	/**
	 * 此处使用try catch
	 *  进行添加信息
	 */
	public void addRemove(HrRemove move) {
		try {
			move.setHrRemStatus("待审核");
			reMoveDao.addRemove(move);
			//HrEmployee employee = reMoveDao.employeeGetId(move.getHrEmployee().getHrEmpId());
			//employee.setHrDepartment(move.getHrDepartment());
			//employee.setHrMajor(move.getHrMajor());
			//reMoveDao.updateEmployee(employee);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// 调用Dao层实现模糊查询方法
	public List<HrEmployee> blurQuery(QueryProperties qp) {
		return (List<HrEmployee>)reMoveDao.blurQuery(qp);
	}
	// 调用Dao层实现待审核方法
	public List<HrRemove> stayAuditing() {
		return (List<HrRemove>)reMoveDao.stayAuditing();
	}
	// 调用Dao层实现根据id查询方法
	public HrRemove remove(Integer id) {
		return reMoveDao.remove(id);
	}
	// 调用Dao层实现新薪酬标准
	public List<HrSalary> selectSalary() {
		return (List<HrSalary>)reMoveDao.selectSalary();
	}
	// 调用Dao层实现薪水id查询
	public HrSalary getByIdSalary(Integer id) {
		return reMoveDao.getByIdSalary(id);
	}
	// 调用Dao层实现根据id查询
	public HrEmployee employeeGetId(Integer id) {
		return reMoveDao.employeeGetId(id);
	}
	// 调用Dao层实现档案信息修改
	public void updateEmployee(HrEmployee employee) {
		reMoveDao.updateEmployee(employee);
	}
	// 调用Dao层实现调动查询
	public List<HrRemove> sMove(QueryProperties qp) {
		return (List<HrRemove>)reMoveDao.sMove(qp);
	}
}
