package com.rs.user.dao;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrUser;

public interface UserDao extends CommonDao {

	//User进行登录方法定义
	public HrUser loginUser(int hrUserId,String hrUserPwd);
}
