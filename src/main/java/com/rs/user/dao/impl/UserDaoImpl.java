package com.rs.user.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrUser;
import com.rs.user.dao.UserDao;

 
public class UserDaoImpl extends CommonDaoImpl implements UserDao {

	/*
	 * 作者:王运发
	 * 时间 2013年11月5日 16:09:51
	 */

	//用户登录
	public HrUser loginUser(int hrUserId, String hrUserPwd) {
		String hql = "from HrUser where hrEmpId=? and hrUserPwd=?";
		List<HrUser> user=getHibernateTemplate().find(hql,hrUserId,hrUserPwd);
		if(user.size()>0 && user!=null)
			return user.get(0);
		else
		return null;
	}
}
