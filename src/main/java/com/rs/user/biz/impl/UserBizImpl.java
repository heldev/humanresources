package com.rs.user.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrUser;
import com.rs.user.biz.UserBiz;
import com.rs.user.dao.UserDao;

public class UserBizImpl implements UserBiz {

	/*
	 * 作者:王运发
	 * 时间 2013年11月5日 16:09:51
	 */

	//根据spring进行Dao层注入
	private UserDao userDao;
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	//用户登录
	public HrUser loginUser(int hrUserId, String hrUserPwd) {
		return userDao.loginUser(hrUserId, hrUserPwd);
	}
	//增加用户
	public void addHrUser(HrUser user) {
		// TODO Auto-generated method stub
		userDao.addEntity(user);
		
	}
	//删除用户
	public void deleteHrUserById(Integer id) {
		// TODO Auto-generated method stub
		userDao.deleteEntityById(HrUser.class, id);
		
	}
	//分页查询
	public List<HrUser> getUsers(FenYe fenYe) {
		// TODO Auto-generated method stub
		return userDao.getList(HrUser.class, fenYe);
	}
	//修改用户
	public void updateHrUserById(HrUser user ) {
		// TODO Auto-generated method stub
		 userDao.updateEntity(user);
	}
	//单查用户
	public HrUser getUserById(Integer id) {
		// TODO Auto-generated method stub
		return (HrUser) userDao.getEntityById(HrUser.class, id);
	}

}
