package com.rs.user.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrUser;

public interface UserBiz {
	//User进行登录方法定义
	public HrUser loginUser(int hrUserId,String hrUserPwd);
	//增加用户
	public void addHrUser(HrUser user);
	//删除用户
	public void deleteHrUserById(Integer id);
	//修改用户
	public void updateHrUserById(HrUser user );
	//分页查询
	public List<HrUser>getUsers(FenYe fenYe);
	//单查用户
	public HrUser getUserById(Integer id);
}
