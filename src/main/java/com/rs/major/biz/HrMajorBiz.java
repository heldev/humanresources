package com.rs.major.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrMajor;
/*
 * 作者: 王运发
 * 时间:2013-11-05 14:09
 */
public interface HrMajorBiz {
	//增加职位
	public void addHrMajor(HrMajor ma);
	//全查职位
	public List<HrMajor> getAllHrMajor(FenYe fenYe);
	//删除职位
	public void deleteHrMajorById(Integer id);
	//得到所有的职位
	public List<HrMajor> getAllHrMajor();
	//单查职位信息
	public HrMajor getHrMahorById(Integer id);

}
