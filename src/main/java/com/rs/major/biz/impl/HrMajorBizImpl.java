package com.rs.major.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrMajor;
import com.rs.major.biz.HrMajorBiz;
import com.rs.major.dao.HrMajorDao;
/*
 * 作者: 王运发
 * 时间:2013-11-05 14:09
 */
public class HrMajorBizImpl implements HrMajorBiz {
	private HrMajorDao hrMajorDao;
	

	public void setHrMajorDao(HrMajorDao hrMajorDao) {
		this.hrMajorDao= hrMajorDao;
	}

	//全查所有的职位
	public List<HrMajor> getAllHrMajor(FenYe fenYe) {
		// TODO Auto-generated method stub
		return  hrMajorDao.getList(HrMajor.class, fenYe);
	}
   //增加新的职位
	public void addHrMajor(HrMajor ma){
		// TODO Auto-generated method stub
		hrMajorDao.addEntity(ma);
	}
   //删除职位
	public void deleteHrMajorById(Integer id) {
		// TODO Auto-generated method stub
		hrMajorDao.deleteEntityById(HrMajor.class, id);
		
	}
  //得到所有的职位
	public List<HrMajor> getAllHrMajor() {
		// TODO Auto-generated method stub
		return hrMajorDao.getAllHrMajor();
	}
//单查职位信息
	public HrMajor getHrMahorById(Integer id) {
		// TODO Auto-generated method stub
		return (HrMajor) hrMajorDao.getEntityById(HrMajor.class,id );
	}

}
