package com.rs.exam.biz.impl;

import java.util.List;

import com.rs.common.entity.HrQuestions;
import com.rs.common.entity.HrQuestionsKind;
import com.rs.exam.biz.QuestionsBiz;
import com.rs.exam.dao.QueryProperties;
import com.rs.exam.dao.QuestionsDao;

public class QuestionsBizImpl implements QuestionsBiz {

	// 调用Dao层进行注入
	private QuestionsDao questionsDao;
	// 进行注入方法定义
	public void setQuestionsDao(QuestionsDao questionsDao) {
		this.questionsDao = questionsDao;
	}
	
	//调用Dao层方法进行实现
	public void addQuestions(HrQuestions question) {
		questionsDao.addQuestions(question);
	}
	//调用Dao层试题种类访法
	public List<HrQuestionsKind> selectKindName() {
		return (List<HrQuestionsKind>)questionsDao.selectKindName();
	}
	//调用Dao层试题查询方法
	public List<HrQuestions> selectByName(QueryProperties qp) {
		return (List<HrQuestions>)questionsDao.selectByName(qp);
	}
	// 调用Dao层根据id进行查询方法
	public HrQuestions getQuestionsById(Integer id) {
		return questionsDao.getQuestionsById(id);
	}
	// 调用Dao层试题全查方法
	public List<HrQuestions> selectAll() {
		return (List<HrQuestions>)questionsDao.selectAll();
	}
	// 调用Dao层方法进行修改
	public void updateExam(HrQuestions question) {
		questionsDao.updateExam(question);
	}
}
