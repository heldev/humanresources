package com.rs.exam.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.rs.common.entity.HrQuestions;
import com.rs.common.entity.HrQuestionsKind;
import com.rs.exam.dao.QueryProperties;
import com.rs.exam.dao.QuestionsDao;

public class QuestionsDaoImpl extends HibernateDaoSupport implements QuestionsDao {

	// 进行添加方法实现
	public void addQuestions(HrQuestions question) {
		getHibernateTemplate().save(question);
	}
	// 试题种类方法实现
	public List<HrQuestionsKind> selectKindName() {
		List<HrQuestionsKind> questionsKind=getHibernateTemplate().find("from HrQuestionsKind");
		return questionsKind;
	}
	// 试题查询方法实现
	public List<HrQuestions> selectByName(QueryProperties qp) {
		// 如果查询条件不为空的话进行根据条件进行全部查询
		if(qp.getEndTime()!=null && qp.getHrKindName()!=null && qp.getHrQueTime()!=null){
			List<HrQuestions> selectByName = getHibernateTemplate().find("from HrQuestions where hrQueTime  between to_date('"+qp.getHrQueTime()+"','yyyy-mm-dd') and to_date('"+qp.getEndTime()+"','yyyy-mm-dd') and  hrQuestionsKind.hrKindId=?",new Object[]{qp.getHrKindName()});
			return selectByName;
		}
		// 如果查询条件都为空的话全查
		else if(qp.getEndTime()==null && qp.getHrKindName()==null && qp.getHrQueTime()==null){
			List<HrQuestions> selectByName = getHibernateTemplate().find("from HrQuestions");
			return selectByName;
		}
		// 如果开始时间和结束时间不为空的话进行查询
		else if(qp.getEndTime()!=null && qp.getHrQueTime()!=null){
			// 标注：使用between and 似乎不能使用？
			List<HrQuestions> selectByName = getHibernateTemplate().find("from HrQuestions  where hrQueTime between to_date('"+qp.getHrQueTime()+"','yyyy-mm-dd') and to_date('"+qp.getEndTime()+"','yyyy-mm-dd')");
			return selectByName;
		}
		/*// 如果查询条件为空的话默认查询根据试题种类进行所有试题查询
		if(!qp.getHrKindName().equals("")){
			List<HrQuestions> selectByName = getHibernateTemplate().find("from HrQuestions hs where hs.hrQuestionsKind.hrKindId=?",qp.getHrKindName());
			return selectByName;
		}*/
		return null;
	}
	// 根据Id查询方法实现
	public HrQuestions getQuestionsById(Integer id) {
		List<HrQuestions> hrQuestions= getHibernateTemplate().find("from HrQuestions hs where hs.hrQueId=?",id);
		return hrQuestions.get(0);
	}
	
	// 试题全查实现
	public List<HrQuestions> selectAll() {
		return (List<HrQuestions>)getHibernateTemplate().find("from HrQuestions");
	}
	
	// 定义试题修改方法实现
	public void updateExam(HrQuestions question) {
		getHibernateTemplate().update(question);
	}
}
