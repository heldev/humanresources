package com.rs.exam.dao;

import java.sql.Date;

public class QueryProperties {

	// 此类作用为进行模块封装
	private Integer hrKindName; //试题分类
	private Date hrQueTime; //起止日期
	private Date endTime; //截止日期
	
	/**
	 * 此处为封装get set方法
	 */
	
	public Date getHrQueTime() {
		return hrQueTime;
	}
	public Integer getHrKindName() {
		return hrKindName;
	}
	public void setHrKindName(Integer hrKindName) {
		this.hrKindName = hrKindName;
	}
	public void setHrQueTime(Date hrQueTime) {
		this.hrQueTime = hrQueTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	
}
