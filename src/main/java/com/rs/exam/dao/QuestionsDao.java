package com.rs.exam.dao;

import java.util.List;

import com.rs.common.entity.HrQuestions;
import com.rs.common.entity.HrQuestionsKind;

public interface QuestionsDao {

	// 添加方法定义
	public void addQuestions(HrQuestions question);
	
	// 定义试题种类方法
	public List<HrQuestionsKind> selectKindName();
	
	// 根据条件进行查询
	public List<HrQuestions> selectByName(QueryProperties qp);
	
	// 定义id查询明细方法
	public HrQuestions getQuestionsById(Integer id);
	
	// 定义试题全查方法
	public List<HrQuestions> selectAll();
	
	// 定义试题修改方法
	public void updateExam(HrQuestions question);
}
