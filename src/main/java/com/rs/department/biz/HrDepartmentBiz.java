package com.rs.department.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrDepartment;

/*
 * 作者： 王运发
 * 时间： 2013-10-30 14:40
 */
 
public interface HrDepartmentBiz {

	//增加部门
	public void addHrDepartment(HrDepartment hrDepartment);
	//修改部门信息
	public void updateHrDepartment(HrDepartment hrDepartment);
	//单查部门信息
	public HrDepartment getHrDepartmentById(Integer id);
	//删除部门信息
	public void  deleteHrDepartmentById(Integer id);
	//全查部门信息
	public List<HrDepartment>  getAllHrDepartment(FenYe fenYe);
	
	//查询所有的部门
	public List<HrDepartment>getAllHrDepartment();
}
