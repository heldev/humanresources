package com.rs.department.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrDepartment;
import com.rs.department.biz.HrDepartmentBiz;
import com.rs.department.dao.HrDepartmentDao;

public class HrDepartmentBizImpl implements HrDepartmentBiz {
	/*
	 * 作者： 王运发
	 * 时间： 2013-10-30  15:03
	 */
	
	//依赖注入
	private HrDepartmentDao hrDepartmentDao;
	public void setHrDepartmentDao(HrDepartmentDao hrDepartmentDao) {
		this.hrDepartmentDao = hrDepartmentDao;
	}
	//增加部门信息
	public void addHrDepartment(HrDepartment hrDepartment) {
		// TODO Auto-generated method stub
		hrDepartmentDao.addEntity(hrDepartment);
	}
    //删除部门信息
	public void deleteHrDepartmentById(Integer id) {
		// TODO Auto-generated method stub
		hrDepartmentDao.deleteEntityById(HrDepartment.class, id);
	}
   //单查部门信息
	public HrDepartment getHrDepartmentById(Integer id) {
		// TODO Auto-generated method stub
		return (HrDepartment) hrDepartmentDao.getEntityById(HrDepartment.class, id);
	}
   //修改部门信息
	public void updateHrDepartment(HrDepartment hrDepartment) {
		// TODO Auto-generated method stub
		hrDepartmentDao.updateEntity(hrDepartment);

	}
	//分页查询
	public List<HrDepartment> getAllHrDepartment(FenYe fenYe) {
		// TODO Auto-generated method stub
		return hrDepartmentDao.getList(HrDepartment.class,fenYe) ;
	}
	//全查
	public List<HrDepartment> getAllHrDepartment() {
		// TODO Auto-generated method stub
		return hrDepartmentDao.getAllHrDepartment();
	}

}
