package com.rs.cultivate.action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrTrain;
import com.rs.common.entity.HrTrainPersonnel;
import com.rs.cultivate.biz.HrTrainBiz;
import com.rs.cultivate.biz.HrTrainPersonnelBiz;
import com.rs.department.biz.HrDepartmentBiz;
import com.rs.files.biz.EmployeeBiz;

public class HrTrainPersonnelAction extends ActionSupport {
	private HrTrainPersonnelBiz hrTrainPersonnelBiz;
	private HrTrainPersonnel personnel;
	private Integer id;
	private List<HrTrainPersonnel> arr;
	private FenYe fenYe;
	private HrDepartmentBiz hrDepartmentBiz;
	private EmployeeBiz employeeBiz;
	private HrTrainBiz hrTrainBiz;
	private List<HrDepartment> list;
	private List<HrEmployee> employees;
	private List<HrTrain> trains;
	private List<Dojo> dos;
	private Integer empid;

	public void setEmpid(Integer empid) {
		this.empid = empid;
	}

	public List<Dojo> getDos() {
		return dos;
	}

	public List<HrTrain> getTrains() {
		return trains;
	}

	public void setHrTrainBiz(HrTrainBiz hrTrainBiz) {
		this.hrTrainBiz = hrTrainBiz;
	}

	public List<HrTrainPersonnel> getArr() {
		return arr;
	}

	public List<HrDepartment> getList() {
		return list;
	}

	public List<HrEmployee> getEmployees() {
		return employees;
	}

	public void setEmployeeBiz(EmployeeBiz employeeBiz) {
		this.employeeBiz = employeeBiz;
	}

	public void setHrDepartmentBiz(HrDepartmentBiz hrDepartmentBiz) {
		this.hrDepartmentBiz = hrDepartmentBiz;
	}

	public FenYe getFenYe() {
		return fenYe;
	}

	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public HrTrainPersonnel getPersonnel() {
		return personnel;
	}

	public void setPersonnel(HrTrainPersonnel personnel) {
		this.personnel = personnel;
	}

	public void setHrTrainPersonnelBiz(HrTrainPersonnelBiz hrTrainPersonnelBiz) {
		this.hrTrainPersonnelBiz = hrTrainPersonnelBiz;

	}

	public String add() {
		try {
			HrEmployee ep = employeeBiz.getHrEmployeeById(empid);
			HrTrain tr = hrTrainBiz.getHrTrainById(id);
			personnel.setHrEmployee(ep);
			personnel.setHrTrain(tr);
			hrTrainPersonnelBiz.addHrTrainPersonnel(personnel);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}

	}

	public String update() {
		try {
			hrTrainPersonnelBiz.upadteHrTrainPersonnel(personnel);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}

	public String cha() {
		try {
			personnel = hrTrainPersonnelBiz.getHrTrainPersonnelById(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}

	public String list() {
		try {
			trains = hrTrainBiz.getAll();
			employees = employeeBiz.selectEmployee();
			list = hrDepartmentBiz.getAllHrDepartment();
			arr = hrTrainPersonnelBiz.getList(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String delete() {
		try {
			hrTrainPersonnelBiz.deleteHrTrainPersonnel(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}

	public String like() {
		employees = employeeBiz.getHrEmployeeBydepartment();
		dos = new ArrayList<Dojo>();
		System.out.println(111);
		for (int i = 0; i < employees.size(); i++) {
			Dojo dj = new Dojo();
			dj.setId(employees.get(i).getHrEmpId());
			dj.setName(employees.get(i).getHrEmpName());
			dos.add(dj);
		}
		return NONE;
	}

}
