package com.rs.cultivate.action;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrTrain;
import com.rs.cultivate.biz.HrTrainBiz;

public class HrTrainAction extends ActionSupport {
	private HrTrainBiz hrTrainBiz;
	private List<HrTrain>arr;
	private FenYe fenYe;
	private HrTrain tr;
	private Integer id;
	public void setId(Integer id) {
		this.id = id;
	}
	public HrTrain getTr() {
		return tr;
	}
	public void setTr(HrTrain tr) {
		this.tr = tr;
	}
	public FenYe getFenYe() {
		return fenYe;
	}
	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}
	public List<HrTrain> getArr() {
		return arr;
	}
	public void setHrTrainBiz(HrTrainBiz hrTrainBiz) {
		this.hrTrainBiz = hrTrainBiz;
	}
	public String list(){
		try {
		 arr=hrTrainBiz.getList(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String add(){
		try {
			hrTrainBiz.addHrTraion(tr);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return ERROR;
		}
	}
	public String update(){
		try {
			hrTrainBiz.updateHrTraion(tr);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return ERROR;
		}
	}
	public String delete(){
		try {
			hrTrainBiz.deleteHrTrain(id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return ERROR;
		}
	}
	public String cha(){
		try {
		tr=	hrTrainBiz.getHrTrainById(id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return ERROR;
		}
	}

}
