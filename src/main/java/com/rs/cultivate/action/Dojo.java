package com.rs.cultivate.action;

public class Dojo {

	private int id;
	private String name;
	public Dojo() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Dojo(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
}
