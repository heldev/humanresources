package com.rs.cultivate.dao.impl;

import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrTrain;
import com.rs.cultivate.dao.HrTrainDao;

public class HrTrainDaoImpl extends CommonDaoImpl implements HrTrainDao {
	/**
	 * 作者:王运发
	 * 时间:2013年11月11日 16:10:12
	 */
	//全查所有的培训项目
	public List<HrTrain> getAll(){
		return this.getHibernateTemplate().find("from HrTrain");
	}
}
