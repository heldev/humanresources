package com.rs.cultivate.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrTrain;

public interface HrTrainDao extends CommonDao{
	/**
	 * 作者:王运发
	 * 时间:2013年11月11日 16:10:12
	 */
	//全查所有的培训项目
	public List<HrTrain> getAll();
}
