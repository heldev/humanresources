package com.rs.cultivate.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrTrainPersonnel;
import com.rs.cultivate.biz.HrTrainPersonnelBiz;
import com.rs.cultivate.dao.HrTrainPersonnelDao;

public class HrTrainPersonnelBizImpl implements HrTrainPersonnelBiz {

	private  HrTrainPersonnelDao hrTrainPersonnelDao;
	
	public void setHrTrainPersonnelDao(HrTrainPersonnelDao hrTrainPersonnelDao) {
		this.hrTrainPersonnelDao = hrTrainPersonnelDao;
	}
	//增加培训记录
	public void addHrTrainPersonnel(HrTrainPersonnel hrp) {
		// TODO Auto-generated method stub
		hrTrainPersonnelDao.addEntity(hrp);
	}
   //删除培训记录
	public void deleteHrTrainPersonnel(Integer id) {
		// TODO Auto-generated method stub
		hrTrainPersonnelDao.deleteEntityById(HrTrainPersonnel.class, id);
	} 
   //单查培训记录
	public HrTrainPersonnel getHrTrainPersonnelById(Integer id) {
		// TODO Auto-generated method stub
		return (HrTrainPersonnel) hrTrainPersonnelDao.getEntityById(HrTrainPersonnel.class, id);
	}
  //分页查询培训记录
	public List<HrTrainPersonnel> getList(FenYe fenYe) {
		// TODO Auto-generated method stub
		return hrTrainPersonnelDao.getList(HrTrainPersonnel.class, fenYe);
	}
  //修改培训记录
	public void upadteHrTrainPersonnel(HrTrainPersonnel hrp) {
		// TODO Auto-generated method stub
		hrTrainPersonnelDao.updateEntity(hrp);
	}
	

}
