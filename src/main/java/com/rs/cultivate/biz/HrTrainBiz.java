package com.rs.cultivate.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrTrain;

public interface HrTrainBiz {
	/**
	 * 作者:王运发
	 * 时间:2013年11月11日 16:10:12
	 */
	//添加培训项目
	public void addHrTraion(HrTrain tr);
	//修改培训项目
	public void updateHrTraion(HrTrain tr);
	//删除培训项目
	public void deleteHrTrain(Integer id);
	//单查培训项目
	public HrTrain getHrTrainById(Integer id);
	//分页查询培训项目
	public  List<HrTrain> getList(FenYe fenYe);
	//全查所有的培训项目
	public List<HrTrain> getAll();
}
