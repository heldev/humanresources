package com.rs.resume.biz.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.rs.common.entity.HrResume;
import com.rs.resume.biz.ResumeBiz;
import com.rs.resume.dao.QueryProperties;
import com.rs.resume.dao.ResumeDao;

public class ResumeBizImpl implements ResumeBiz {

	//进行Dao层注入
	private ResumeDao resumeDao;
	
	//封住输入
	public void setResumeDao(ResumeDao resumeDao) {
		this.resumeDao = resumeDao;
	}
	
	//进行Dao层方法实现
	public void addResume(HrResume resume) {
			resume.setHrPerStatus("待审核");
			resumeDao.addResume(resume);
	}
	//调用Dao层方法进行模糊查询
	public List<HrResume> getVague(QueryProperties qp) {
		return resumeDao.getVague(qp);
	}
	// 调用Dao层方法进行全查
	public List<HrResume> selectAll() {
		return (List<HrResume>)resumeDao.selectAll();
	}
	// 调用Dao层方法进行id查询
	public HrResume hrResumeGetById(Integer id) {
			return resumeDao.hrResumeGetById(id);
	}
	// 调用Dao层方法进行修改
	public void updateResume(HrResume resume) {
		resumeDao.updateResume(resume);
	}
	// 调用Dao层实现简历查询
	public List<HrResume> validResume(QueryProperties qp) {
		return (List<HrResume>)resumeDao.validResume(qp);
	}

}
