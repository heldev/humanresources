package com.rs.resume.biz;

import java.util.List;

import com.rs.common.entity.HrResume;
import com.rs.resume.dao.QueryProperties;

public interface ResumeBiz {

	//进行简历方法定义
	public void addResume(HrResume resume);
	
	//进行模糊查询发法定义
	public List<HrResume> getVague(QueryProperties qp);
	
	// 定义全查方法
	public List<HrResume> selectAll();
	
	// 定义id查询方法
	public HrResume hrResumeGetById(Integer id);
	
	// 定义修改方法
	public void updateResume(HrResume resume);
	
	// 定义简历查询
	public List<HrResume> validResume(QueryProperties qp);
}
